from openfoam import *

#linear (i*128)
#config.table[P.MESH_SIZE] = [ 64, 192, 320, 448, 576, 704, 832, 960, 1088] 
#exp
#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)

config = OpenFOAM_Config(session_path, 
                         "pics",
                         "interDyMFoam",
                         "testCases/2.4.x-damBreakWithObstacle")
#                         "testCases/2.4.x-damBreakWithObstacle-GAMG")

config.samples=1
config.table[P.LOAD_MANAGER] = [False]
config.table[P.POWER_MANAGER] = [ False]
config.table[P.POWER_CAP] = [1]
config.table[P.CPU] = [ (4,8)]
config.table[P.ENDTIME] = [1]
config.table[P.BALANCE_PERIOD] = [6]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]
config.table[P.HETEROGENEITY] = [0]
batch = openfoam_loadManager(config)
batch.run(False)
batch.run(True)

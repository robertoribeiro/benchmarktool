from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 0.100
globalBalanceP = 6
globalMeshType = "dynamicRefineExtFvMesh"
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"
gStrontSize = 2048

compareNode="r662"
compareNodeCPUs=24

######################################################################



config = OpenFOAM_Config(session_path, 
                         compareNode,
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [False]
config.table[P.COMM_GRAPH] = [False]
config.table[P.CPU] = [ (compareNodeCPUs, gStrontSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=[compareNode] 
config.netInterface = "tcp"
config.nodes=1
config.walltimeH=5

batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)

######################################################################

config = OpenFOAM_Config(session_path,
                         "quarter-knl",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [False]
config.table[P.COMM_GRAPH] = [False]
config.table[P.CPU] = [  (16,gStrontSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=[]
config.nodes=0
config.walltimeH=6

#batch = openfoam_loadManager_pbs_knl(config)
#batch.run(False)
#batch.run(True)


######################################################################

config = OpenFOAM_Config(session_path, 
                         "half-knl",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [False]
config.table[P.COMM_GRAPH] = [False]
config.table[P.CPU] = [ (32,gStrontSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=[]
config.nodes=0
config.walltimeH=6

batch = openfoam_loadManager_pbs_knl(config)
batch.run(False)
batch.run(True)


######################################################################

config = OpenFOAM_Config(session_path, 
                         "knl",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [False]
config.table[P.COMM_GRAPH] = [False]
config.table[P.CPU] = [ (64,gStrontSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=[]
config.nodes=0
config.walltimeH=6

batch = openfoam_loadManager_pbs_knl(config)
batch.run(False)
batch.run(True)


######################################################################


config = OpenFOAM_Config(session_path,
                         "641+quarter-knl",
                         globalSolver,
                         globalCase)

config.samples=4
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.COMM_GRAPH] = [True]
config.table[P.CPU] = [ (16+16,gStrontSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r641"]
config.nodes=1
config.walltimeH=5

#batch = openfoam_loadManager_pbs_knl(config)
#batch.run(False)
#batch.run(True)


######################################################################


config = OpenFOAM_Config(session_path, 
                         compareNode+"+half-knl",
                         globalSolver,
                         globalCase)

config.samples=7
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.COMM_GRAPH] = [True]
config.table[P.CPU] = [ (compareNodeCPUs+32,gStrontSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=[compareNode] 
config.nodes=1
config.walltimeH=5

batch = openfoam_loadManager_pbs_knl(config)
batch.run(False)
batch.run(True)


######################################################################


config = OpenFOAM_Config(session_path, 
                         compareNode+"+knl",
                         globalSolver,
                         globalCase)

config.samples=4
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.COMM_GRAPH] = [True]
config.table[P.CPU] = [ (compareNodeCPUs+64,gStrontSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=[compareNode] 
config.nodes=1
config.walltimeH=5

batch = openfoam_loadManager_pbs_knl(config)
batch.run(False)
batch.run(True)








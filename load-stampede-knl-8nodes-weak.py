from openfoam import *


#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]


session_path = prepare_folder_structure(__file__)
globalEndTime = 0.100
globalBalanceP = 6
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"

config = OpenFOAM_Config(session_path,
                         "dyn",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(64, 128), (128, 256), (256, 512), (512, 1024)]
#config.table[P.CPU] = [(1, 2), (8, 16), (16, 32), (32, 64)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]


config.nodes=8
config.queue="normal"
batch = openfoam_loadManager_sbatch(config)
batch.run(False)

from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 0.200
globalBalanceP = 6
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"

globalStrongSize = 512
######################################################################
config = OpenFOAM_Config(session_path,
                         "2nodes",
                         globalSolver,
                         globalCase)

config.samples=5
config.table[P.LOAD_MANAGER] = [False]
config.table[P.POWER_MANAGER] = [True, False]
config.table[P.POWER_CAP] = [0.7, 0.85, 1.0]
config.table[P.CPU] = [(16, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r421"]
config.netInterface = "myri"

batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)


######################################################################


config = OpenFOAM_Config(session_path,
                         "4nodes",
                         globalSolver,
                         globalCase)

config.samples=5
config.table[P.LOAD_MANAGER] = [False]
config.table[P.POWER_MANAGER] = [True, False]
config.table[P.POWER_CAP] = [0.7, 0.85, 1.0]
config.table[P.CPU] = [(32, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r421", "r641", "r421"]
config.netInterface = "myri"

batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)


######################################################################


config = OpenFOAM_Config(session_path,
                         "Test-4nodes",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [False]
config.table[P.POWER_MANAGER] = [False, True]
config.table[P.POWER_CAP] = [1]
config.table[P.CPU] = [(32, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r421", "r641", "r421"]
config.netInterface = "myri"

#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)


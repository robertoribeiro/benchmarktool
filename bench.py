import re
import time
import os
import smtplib
import socket
import shutil
import inspect
import sys
import pprint
from pushbullet import Pushbullet
from abc import ABCMeta, abstractmethod

# Auxiliar core routines

def prepare_folder_structure(session):
    """Ensures that absoluteroot/benchmarks/session.timetag folder is created. 
    Also creates a log file descriptor used for all the logging defined as a
     global var log_file_fd. Returns the path to session folder"""
     
    
    session=  os.path.basename(session).replace(".py", "")
    work_home = os.path.dirname(os.path.abspath(__file__))
    if not os.path.exists(work_home + "/benchmarks"):
        os.mkdir(work_home + "/benchmarks")

    host = socket.gethostname()
    
    session_working_folder = work_home + "/" + "benchmarks/" +  host + "-" + session + "." + str(int(round(time.time() * 1000)))
    os.mkdir(session_working_folder)
    
    shutil.copy(inspect.getfile(sys._getframe(1)), session_working_folder +"/main.py")
       

    return session_working_folder


def inplace_change(filename, old_string, new_string):
        s = open(filename).read()
        # if old_string in s:
        s = re.sub(old_string, new_string, s)
        f = open(filename, 'w')
        f.write(s)
        f.flush()
        f.close()
        
    
def send_email(v, body):
            FROM = 'benchmarkTool@' + socket.gethostname()
            TO = ['rbrt.ribeiro@gmail.com']  # must be a list
            SUBJECT = str(v)
            TEXT = str(body)
            # Prepare actual message
            message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
            """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
            try:
                server = smtplib.SMTP("localhost")  # or port 465 doesn't seem to work!
                server.ehlo()
                server.sendmail(FROM, TO, message)
                server.close()
            except:
                print("failed to send mail")
                
def send_pushbullet(v, body):
    key = open("./pushbullet.key" , 'r').read()
    if len(key) != 34 : key = str(key[:-1])
    Pushbullet(key).push_note('benchmarkTool@' + socket.gethostname() , v+ "\n\n"  + body)

# End Auxiliar core routines

class BenchEntry:
    """ Core base class for an entry
    Implements the main loop method only based on overrided methods 
    
    """
    __metaclass__ = ABCMeta
        
    def __init__(self, name, session_path, max_samples, parameterNames,
                 parameterMatrix):
        self.session_path = session_path
        self.name = name
        self.entry_path = session_path + "/" + self.name
        self.current_iteration_folder = ""
        self.sample_number= 0
        self.max_samples = max_samples
        self.parameterNames = parameterNames
        self.number_of_parameters = len(parameterNames)
        self.failCount=0
        
        self.log_file_fd = open(session_path + "/log", 'a')
        
#TODO Assert number of parameters
        self.parameterMatrix = parameterMatrix
        self.current_parameter_index = [0] * self.number_of_parameters
        if not os.path.exists(self.entry_path):
            os.mkdir(self.entry_path)
        
    def myLog(self, v):
        
        self.log_file_fd.write(str(v) + "\n")
        self.log_file_fd.flush()
        print(str(v))

    def run(self, post):
        self.current_parameter_index = [0] * self.number_of_parameters
        
        single = False
        if self.number_of_parameters == 0:
            single = True
            
        while single or self.iterate():
            self.current_iteration_folder = self.entry_path + "/" + self.get_iteration_str()
            if not post: os.mkdir(self.current_iteration_folder)
            if not post: self.mutate()
            while self.sample():
                if not post: sample_folder = self.current_iteration_folder + "/" + self.get_sample_str()
                if not post: self.execute(sample_folder)
                self.finalize_sample()
            self.finalize_iteration(post)
            if single : break
        self.finalize_entry(post)
        
    @abstractmethod
    def execute(self, working_directory):
        """
        Launches application in the provided working directory.
        Must ensure argument and/or input folder specification
        """ 
        pass
            
    @abstractmethod    
    def mutate(self): 
        """
        Apply the required changes to iterate the benchmark,
        e.g. increase the number of processors or work size
        """
        pass
    
    def iterate(self):
        """
        Return a bool that determines if the benchmark is finished and all 
        configurations coverered
        """ 
        
        if self.number_of_parameters == 0 : return False
        
        if self.current_parameter_index[-1] >= len(self.parameterMatrix[-1]):
            return False
        else:
            return True
    
    def sample(self):
        """
        Returns a bool that dictates if we stop sampling
        """ 
        if self.sample_number >= self.max_samples:
            return False
        else:
            return True
    
    
    def finalize_iteration(self, post):
        if post:
            self.pre_finalize_iteration()
        self.core_finalize_iteration(0)
        if post:
            self.post_finalize_iteration()
        
        self.sample_number = 0

    def core_finalize_iteration(self, n):
          
          
        if len(self.current_parameter_index) == 0 : return
        if n == 0:
            self.current_parameter_index[n]+=1

        if n >= self.number_of_parameters:
            return  
        if (self.current_parameter_index[n] >= len(self.parameterMatrix[n])):
            if n+1 < len(self.current_parameter_index):
                self.current_parameter_index[n] = 0
                self.current_parameter_index[n+1] += 1
                self.core_finalize_iteration(n+1)
        
#         self.first_field_index += 1
#         
#         if (self.first_field_index >= len(self.first_field)):
#             self.first_field_index = 0
#             self.second_field_index += 1
#             
#             if (self.second_field_index >= len(self.second_field)):
#                 self.second_field_index = 0
#                 self.third_field_index += 1
#             
#                 if (self.third_field_index >= len(self.third_field)):
#                     self.third_field_index = 0       
#                     self.fourth_field_index += 1
#                     
    
    @abstractmethod    
    def pre_finalize_iteration(self):
        """
        Invoked in the end of each iteration. Responsible for parsing and organizing
        the output
        pre_finalize_iteration()
        core_finalize_iteration()
        post_finalize_iteration()
        """     
        pass
    
    @abstractmethod    
    def post_finalize_iteration(self):
        """
        Invoked in the end of each iteration. Responsible for parsing and organizing
        the output
        pre_finalize_iteration()
        core_finalize_iteration()
        post_finalize_iteration()
        """     
        pass
       
    def finalize_sample(self):
        """
        Invoked in the end of each sample. 
        """ 
        self.sample_number += 1
    
    def finalize_entry(self, post):
        """
        Invoked in the end of the benchmark. 
        """ 
        if post:
            f = open(self.entry_path + "/" + self.name + "-results.txt", 'w')
            a=self.pretty_print()
            f.write(a)
            f.write("\n\n")
        
        
        f = open(self.entry_path + "/" + self.name + "-config.txt", 'w')    
        for aa, vv in self.initial_config.table.iteritems():
            f.write(str(aa.value)+"="+str(vv)+"\n")
            
        #send_email(os.path.basename(self.session_path) + "-" + self.name + " test done",a )
        if post and self.initial_config.pushbullet:
            send_pushbullet(os.path.basename(self.session_path) + "-" + self.name + " test done",a )
            
    
    @abstractmethod 
    def pretty_print(self):
        pass
    
    def getCurrentParameter(self, n):
        return self.parameterMatrix[n][self.current_parameter_index[n]]
    
    def get_iteration_str(self):
        """
        Returns a string that identifies the iteration
        """ 
        result = ""
        for p in range(self.number_of_parameters):
            result += self.parameterNames[p].value + "." + str(self.getCurrentParameter(p))
            if p != (self.number_of_parameters -1):
                result += "_"
        if result == "" : result = "defaults"
        return result.replace(" ","").replace("(","-").replace(")","-")
            
    def get_sample_str(self):
        """
        Returns a string that identifies the sample
        """ 
        return "sample" + str(self.sample_number)

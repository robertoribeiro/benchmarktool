# README #

A benchmark tool that provides a short and simple API to implement an automated benchmark on an application in the context of, but not only, high performance computing.
It provides a structure that tries to be as generic as possible to cover typical benchmarks like execution time with different parameters, speedup with and without optimizations, parallel scalability analysis, among others.
(Very) Basic knowledge of Python and Object Oriented Programming are required.  

### Aplication requirements ###
The application must be able to persistently store the output that regards the benchmark in a file. This can be enforded in the execution method and then parsed in the finalize method.
This will enable a dynamic run sample execution since the loop is controlled by the script. 

### Folder structure ###

* top-folder
    * Session
        * Entry-A
            * Iteration-A
                * Sample-0
                * Sample-1
                * ...
                * Sample-N
            * Iteration-B
            * ...
            * Iteration-N
            * entry-results.txt
        * Entry-B
        * ...
        * Entry-N

### Definitions ###

**Session**
Corresponds to an execution of the script. It's suffixed with a temporal tag. 

**Entry**
Identifies the type of benchmark or the property that will be assessed at high level.

**Iteration**
Represents a change in the input. 

**Sample**
Each execution is repeated in the exact same circumstances.

### Example ###

A typical example is to assess the execution time of the application with different parallel execution configuration (which we will call scalability). For instance with 1, 2 or 3 processors. The folder the structure will be the following:

* bechmark-tool-example
    * scalability
        * 1PROC
            * Sample-0
            * Sample-1
            * Sample-2
        * 2PROC
            * Sample-0
            * Sample-1
            * Sample-2
        * 3PROC
            * Sample-0
            * Sample-1
            * Sample-2
        * entry-results.txt

The entry-results.txt may look like:

1PROC | 2PROC | 3PROC
- | - | -
10s | 7s | 5s
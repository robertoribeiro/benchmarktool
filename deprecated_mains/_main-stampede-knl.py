from openfoam import *


#linear (i*128)
#config.table[P.MESH_SIZE] = [ 64, 192, 320, 448, 576, 704, 832, 960, 1088] 
#exp
#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]


session_path = prepare_folder_structure("stampede-knl")
globalEndTime = 0.300
globalBalanceP = 6
globalMeshType = "dynamicRefineExtFvMesh"
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"
gStrongSize = 1024

######################################################################
        
config = OpenFOAM_Config(session_path, 
                         "1-node-strong",
                         globalSolver,
                         globalCase)

config.samples=1
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(64, gStrongSize),(32, gStrongSize), (16, gStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodes=1
config.queue="normal"
#batch = openfoam_loadManager_sbatch(config)
#batch.run(False)

######################################################################

config = OpenFOAM_Config(session_path, 
                         "2-nodes-strong",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(128, gStrongSize),(64, gStrongSize), (32, gStrongSize), (16, gStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]


config.nodes=2
config.queue="normal"
#batch = openfoam_loadManager_sbatch(config)
#batch.run(False)


######################################################################

config = OpenFOAM_Config(session_path,
                         "3-nodes-strong",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(192, gStrongSize), (96, gStrongSize), (48, gStrongSize), (24, gStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]


config.nodes=3
config.queue="normal"
batch = openfoam_loadManager_sbatch(config)
batch.run(False)

######################################################################


config = OpenFOAM_Config(session_path,
                         "4-nodes-strong",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(256, gStrongSize), (128, gStrongSize), (64, gStrongSize), (32, gStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]


config.nodes=4
config.queue="normal"
batch = openfoam_loadManager_sbatch(config)
batch.run(False)

######################################################################
        
config = OpenFOAM_Config(session_path, 
                         "1proc",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [False]
config.table[P.CPU] = [(1, gStrongSize),(-1, 10)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodes=1
config.queue="normal"
#batch = openfoam_loadManager_sbatch(config)
#batch.run(False)

######################################################################
######################################################################
        
# config = OpenFOAM_Config(session_path, 
#                          "1-node-weak",
#                          globalSolver,
#                          globalCase)
# 
# config.samples=2
# config.table[P.LOAD_MANAGER] = [True, False]
# config.table[P.CPU] = [(64, 128),(32, 64),(16, 32),(1, 2)]
# config.table[P.ENDTIME] = [globalEndTime]
# config.table[P.BALANCE_PERIOD] = [globalBalanceP]
# config.table[P.MESH_TYPE] = [globalMeshType]
# 
# config.failCount=2
# config.nodes=1
# config.queue="development"
# batch = openfoam_loadManager_sbatch(config)
# batch.run(False)


######################################################################

# config = OpenFOAM_Config(session_path, 
#                          "2-nodes-weak",
#                          globalSolver,
#                          globalCase)
# 
# config.samples=2
# config.table[P.LOAD_MANAGER] = [True, False]
# config.table[P.CPU] = [(64, 128),(32, 64),(16, 32),(1, 2)]
# config.table[P.ENDTIME] = [globalEndTime]
# config.table[P.BALANCE_PERIOD] = [globalBalanceP]
# config.table[P.MESH_TYPE] = [globalMeshType]
# 
# config.failCount=2
# config.nodes=2
# config.queue="development"
# batch = openfoam_loadManager_sbatch(config)
# batch.run(False)

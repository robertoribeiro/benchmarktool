from openfoam import *

#linear (i*128)
#config.table[P.MESH_SIZE] = [ 64, 192, 320, 448, 576, 704, 832, 960, 1088] 
#exp
#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure("stampede")
globalEndTime = 0.200
globalBalanceP = 6
globalMeshType = "dynamicRefineExtFvMesh"
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"
globalStrongSize = 256
######################################################################
        
config = OpenFOAM_Config(session_path, 
                         "1-node-strong",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(16, globalStrongSize),(8,globalStrongSize),(4, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodes=1
config.queue="vis"
#batch = openfoam_loadManager_sbatch(config)
#batch.run(False)

######################################################################
        
config = OpenFOAM_Config(session_path, 
                         "2-node-strong",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(32, globalStrongSize), (16, globalStrongSize),(8, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]


config.nodes=2
config.queue="vis"
batch = openfoam_loadManager_sbatch(config)
batch.run(False)

######################################################################

config = OpenFOAM_Config(session_path,
                         "3-node-strong",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(48, globalStrongSize),(32,globalStrongSize),(16, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]


config.nodes=3
config.queue="vis"
batch = openfoam_loadManager_sbatch(config)
batch.run(False)



######################################################################
######################################################################
        
config = OpenFOAM_Config(session_path, 
                         "1-node-weak",
                         globalSolver,
                         globalCase)

config.samples=1
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(16, 256),(8,128),(4, 64),(1, 16)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]


config.nodes=1
config.queue="vis"
#batch = openfoam_loadManager_sbatch(config)
#batch.run(False)


######################################################################
        
config = OpenFOAM_Config(session_path, 
                         "2-node-weak",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(16, 256),(8,128),(4, 64),(1, 16)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]


config.nodes=2
config.queue="vis"
#batch = openfoam_loadManager_sbatch(config)
#batch.run(False)


######################################################################

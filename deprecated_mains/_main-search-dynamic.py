from openfoam import *

#linear (i*128)
#config.table[P.MESH_SIZE] = [ 64, 192, 320, 448, 576, 704, 832, 960, 1088] 
#exp
#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]
#config.nodeList=["ppn=32:r641", "ppn=8:r421"]
#config.nodeList=["ppn=32:r641", "ppn=24:r431"]

session_path = prepare_folder_structure("search-dynamic-homog")
globalEndTime = 0.200
globalBalanceP = 6
globalMeshType = "dynamicRefineExtFvMesh"
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"
globalStrongSize = 256

######################################################################

config = OpenFOAM_Config(session_path, 
                         "1-node-strong",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(4,globalStrongSize), (8,globalStrongSize), (16, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r641"]
config.mindHT="full"
config.netInterface="tcp"

#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)

######################################################################

config = OpenFOAM_Config(session_path, 
                         "2-node-strong",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(16,globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r641", "r641"]
config.mindHT="full"

#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)

######################################################################

config = OpenFOAM_Config(session_path,
                         "3-node-strong",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(24,globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r641", "r641", "r641"]
config.mindHT="full"

#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)

######################################################################
config = OpenFOAM_Config(session_path,
                         "4-node-strong",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(4, globalStrongSize), (8,globalStrongSize), (16,globalStrongSize), (32, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r641", "r641", "r641", "r641"]
config.mindHT="full"

batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)

######################################################################


config = OpenFOAM_Config(session_path,
                         "1proc",
                         globalSolver,
                         globalCase)

config.samples=1
config.table[P.LOAD_MANAGER] = [False]
config.table[P.CPU] = [(1, globalStrongSize), (-1, 10)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r641"]
config.mindHT="full"

batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)


######################################################################

config = OpenFOAM_Config(session_path, 
                         "1-node-weak",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(16,256), (8,128), (1, 8)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r641"]
#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)

######################################################################

config = OpenFOAM_Config(session_path, 
                         "2-node-weak",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(16,256), (8,128), (1, 8)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r641", "r641"]
#openfoam_loadManager_pbs_multi_node(config).run()
#batch.run(False)
#batch.run(True)


######################################################################

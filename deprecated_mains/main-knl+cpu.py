from openfoam import *

#linear (i*128)
#config.table[P.MESH_SIZE] = [ 64, 192, 320, 448, 576, 704, 832, 960, 1088] 
#exp
#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]
#config.nodeList=["ppn=32:r641", "ppn=8:r421"]
#config.nodeList=["ppn=32:r641", "ppn=24:r431"]

session_path = prepare_folder_structure("knl+cpu")
globalEndTime = 0.250
globalBalanceP = 6
globalMeshType = "staticFvMesh"
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"
gStrontSize= 2048

######################################################################


config = OpenFOAM_Config(session_path, 
                         "strong",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [True, False]
#config.table[P.CPU] = [ (8,gStrontSize), (16, gStrontSize), (32,gStrontSize), (48,gStrontSize), (80,gStrontSize)]
config.table[P.CPU] = [(80,gStrontSize), (48,gStrontSize), (32, gStrontSize), (16,gStrontSize), (8,gStrontSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.mindHT="full"
config.nodeList=["r641"] # + 64 cores from KNL
batch = openfoam_loadManager_pbs_knl(config)
batch.run(False)
batch.run(True)

######################################################################


config = OpenFOAM_Config(session_path, 
                         "weak",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [True, False]

config.table[P.CPU] = [(80, gStrontSize), (48,int(48/80.0*gStrontSize)), \
                        (32,int(32/80.0*gStrontSize)),(16, int(16/80.0*gStrontSize))]

config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.mindHT="full"
config.nodeList=["r641"] # + 64 cores from KNL
#batch = openfoam_loadManager_pbs_knl(config)
#batch.run(False)
#batch.run(True)


######################################################################

config = OpenFOAM_Config(session_path,
                         "1proc-multi-core",
                         globalSolver,
                         globalCase)

config.samples=1
config.table[P.LOAD_MANAGER] = [False]
config.table[P.CPU] = [(1, gStrontSize), (1, int(1/80.0*gStrontSize)), (16, gStrontSize), (16,  int(16/80.0*gStrontSize))]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r641"] # + 64 cores from KNL
#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)

config = OpenFOAM_Config(session_path,
                         "knl-only",
                         globalSolver,
                         globalCase)

config.samples=1
config.table[P.LOAD_MANAGER] = [False]
config.table[P.CPU] = [(1, gStrontSize), (1,int(1/80.0*gStrontSize)), (64, gStrontSize), (64,  int(64/80.0*gStrontSize))]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=[] # + 64 cores from KNL
#batch = openfoam_loadManager_pbs_knl(config)
#batch.run(False)
#batch.run(True)



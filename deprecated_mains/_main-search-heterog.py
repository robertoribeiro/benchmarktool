from openfoam import *

#linear (i*128)
#config.table[P.MESH_SIZE] = [ 64, 192, 320, 448, 576, 704, 832, 960, 1088] 
#exp
#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]
#config.nodeList=["ppn=32:r641", "ppn=8:r421"]
#config.nodeList=["ppn=32:r641", "ppn=24:r431"]

session_path = prepare_folder_structure("search-heterog")
globalEndTime = 0.500
globalBalanceP = 6
globalMeshType = "staticFvMesh"
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"
gPushB=True
gStrongSize = 1024

######################################################################

config = OpenFOAM_Config(session_path, 
                         "4-node-strong",
                         globalSolver,
                         globalCase)

config.samples=4
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(8,gStrongSize), (16,gStrongSize), (32,gStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r641", "r421", "r641", "r421"]

config.mindHT="full"
config.pushbullet = gPushB

batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)


######################################################################

config = OpenFOAM_Config(session_path, 
                         "2-node-strong",
                         globalSolver,
                         globalCase)

config.samples=4
config.table[P.LOAD_MANAGER] = [True,False]
config.table[P.CPU] = [(4,gStrongSize), (8, gStrongSize), (16, gStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.mindHT="full"
config.pushbullet = gPushB


config.nodeList=["r641", "r421"]

batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)

######################################################################

config = OpenFOAM_Config(session_path, 
                         "1proc-r641",
                         globalSolver,
                         globalCase)

config.samples=1
config.table[P.LOAD_MANAGER] = [False]
config.table[P.CPU] = [(1,gStrongSize), (-1, 10)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r641"]

config.mindHT="full"

#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)

config = OpenFOAM_Config(session_path, 
                         "1proc-r421",
                         globalSolver,
                         globalCase)

config.samples=1
config.table[P.LOAD_MANAGER] = [False]
config.table[P.CPU] = [(1,gStrongSize), (-1, 10)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r421"]

config.mindHT="full"

#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)

config = OpenFOAM_Config(session_path, 
                         "1proc-r431",
                         globalSolver,
                         globalCase)

config.samples=1
config.table[P.LOAD_MANAGER] = [False]
config.table[P.CPU] = [(1,gStrongSize), (-1, 10)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r431"]

config.mindHT="full"

#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)

######################################################################
######################################################################

config = OpenFOAM_Config(session_path, 
                         "2-node-weak",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.CPU] = [(16,256), (8,128), (1, 8)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.failCount=2
config.nodeList=["r641", "r432"]
#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)

######################################################################

config = OpenFOAM_Config(session_path, 
                         "4-node-weak",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [True,False]
config.table[P.CPU] = [(32,256), (16,128), (8,64), (1,8)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r641", "r432", "r641", "r432"]
#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)


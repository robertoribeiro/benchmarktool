from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 0.200
globalBalanceP = 6
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"

######################################################################
config = OpenFOAM_Config(session_path,
                         "static-hete",
                         globalSolver,
                         globalCase)

config.samples=3
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.COMM_GRAPH] = [True]
config.table[P.CPU] = [(32, 128), (32, 256),(32, 512), (32, 1024)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["staticFvMesh"]

config.nodeList=["r641", "r421", "r641", "r421"]
config.netInterface = "myri"

#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)


######################################################################

config = OpenFOAM_Config(session_path,
                         "dyn-homog",
                         globalSolver,
                         globalCase)

config.samples=1
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.COMM_GRAPH] = [True]
#config.table[P.CPU] = [ (32, 128),(32, 256), (32, 512)]
config.table[P.CPU] = [ (32, 128)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r662", "r662", "r662", "r662"]
config.netInterface = "myri"

batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)

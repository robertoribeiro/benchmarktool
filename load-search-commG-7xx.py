from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 0.100
globalBalanceP = 6
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"

######################################################################
globalStrongSize = 512

config = OpenFOAM_Config(session_path,
                         "2nodes-eth",
                         globalSolver,
                         globalCase)

config.samples=4
config.table[P.LOAD_MANAGER] = [True]
config.table[P.COMM_GRAPH] = [True, False]
#config.table[P.CPU] = [(16, globalStrongSize),(32, globalStrongSize), (64, globalStrongSize)]
config.table[P.CPU] = [(64, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r781", "r781"]
config.netInterface = "tcp"

config.mindHT = "partial"

#HOMOGENEOUS
batch =  openfoam_loadManager_pbs_single_job(config)
batch.run(False)
batch.run(True)

######################################################################
globalStrongSize = 2048

config = OpenFOAM_Config(session_path,
                         "4nodes-eth",
                         globalSolver,
                         globalCase)

config.samples=8
config.table[P.LOAD_MANAGER] = [True]
config.table[P.COMM_GRAPH] = [True, False]
#config.table[P.CPU] = [(16, globalStrongSize),(32, globalStrongSize), (64, globalStrongSize)]
config.table[P.CPU] = [(112, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r781", "r781", "r771", "r771"]
config.netInterface = "tcp"

#config.mindHT = "partial"

#batch =  openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)


######################################################################

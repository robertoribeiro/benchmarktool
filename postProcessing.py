import sys

import numpy
import itertools
import shutil
import subprocess
import time
import os
import re
import signal
import traceback
import collections

from enum import Enum, unique
from collections import OrderedDict
from tabulate import tabulate
from time import sleep

from openfoam import *

from math import ceil
import pickle

for folder in os.listdir(os.curdir):
    if os.path.isfile(folder + "/config.obj"):
        file = open(folder + "/config.obj",'r')
        config= pickle.load(file)
        config.session_path = os.curdir
        config.pushbullet= False
        aa = openfoam_loadManager(config)
        aa.run(True)
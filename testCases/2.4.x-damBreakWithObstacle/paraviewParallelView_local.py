import os
import subprocess
from paraview.simple import *
from colour import Color


colors = list(Color("red").range_to(Color("blue"), 5))


c = len([filtered for filtered in os.listdir(os.getcwd()) if "processor" in filtered ])
colors = list(Color("red").range_to(Color("blue"), c))
colorIt = iter(colors)

print os.listdir(os.getcwd())
for folder in os.listdir(os.getcwd()):
    if "processor" in folder:
        ps = subprocess.Popen(["touch", folder + "/" + folder+".OpenFOAM"], stdout=subprocess.PIPE)
        out, err = ps.communicate()
        
        p0_OpenFOAM = OpenFOAMReader( guiName=folder+".OpenFOAM", Decomposepolyhedra=1, CaseType='Reconstructed Case', MeshRegions=['internalMesh'], Createcelltopointfiltereddata=1, Cachemesh=1, FileName=folder + "/" + folder+".OpenFOAM", LagrangianArrays=[], CellArrays=['U', 'alpha.water', 'alpha.water.org', 'p', 'pd'], LagrangianpositionsareinOF13binaryformat=0, PointArrays=[], Readzones=0, Adddimensionalunitstoarraynames=0, ListtimestepsaccordingtocontrolDict=0 )
        SetActiveSource(p0_OpenFOAM)
                
        a1_alpha1_PiecewiseFunction = CreatePiecewiseFunction( Points=[0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0] )
 
        a1_alpha1_PVLookupTable = GetLookupTableForArray( "alpha.water", 1, Discretize=1, RGBPoints=[0.0, 0.23, 0.299, 0.754, 0.0, 0.706, 0.016, 0.15], UseLogScale=0, VectorComponent=0, NanColor=[0.25, 0.0, 0.0], NumberOfTableValues=256, EnableOpacityMapping=0, ColorSpace='Diverging', IndexedLookup=0, VectorMode='Magnitude', ScalarOpacityFunction=a1_alpha1_PiecewiseFunction, HSVWrap=0, ScalarRangeInitialized=1.0, AllowDuplicateScalars=1, Annotations=[], LockScalarRange=0 )
        
        DataRepresentation1 = Show()
        DataRepresentation1.Representation = 'Surface With Edges'
        DataRepresentation1.ColorArrayName = ('POINT_DATA', 'alpha.water')
        DataRepresentation1.LookupTable = a1_alpha1_PVLookupTable
        DataRepresentation1.EdgeColor =  next(colorIt).get_rgb()
        

          
Render()

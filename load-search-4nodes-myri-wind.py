from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 100
globalBalanceP = 6
globalSolver = "simpleDyMFoam"
globalCase =  "testCases/2.4.0-windAroundBuildings"

######################################################################

config = OpenFOAM_Config(session_path,
                         "static-hete",
                         globalSolver,
                         globalCase)

config.samples=4
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.COMM_GRAPH] = [False]
config.table[P.CPU] = [(4, 4), (8, 4), (16, 4 ), (32, 4)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r421", "r641", "r421"]
config.netInterface = "myri"

config.cellCountMethod = "analytical-buildings"


#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)


######################################################################

config = OpenFOAM_Config(session_path,
                         "dyn-homog",
                         globalSolver,
                         globalCase)

config.samples=4
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.COMM_GRAPH] = [False]
config.table[P.CPU] = [(4, 4), (8, 4), (16, 4 ), (32, 4)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r641", "r641", "r641"]
config.netInterface = "myri"

config.cellCountMethod = "analytical-buildings"


batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)




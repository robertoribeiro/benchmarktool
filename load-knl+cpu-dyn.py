from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 0.100
globalBalanceP = 6
globalMeshType = "dynamicRefineExtFvMesh"
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"
gStrontSize = 1024

######################################################################


config = OpenFOAM_Config(session_path, 
                         "662",
                         globalSolver,
                         globalCase)

config.samples=4
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.COMM_GRAPH] = [False]
config.table[P.CPU] = [ (4,gStrontSize), (8,gStrontSize), (16,gStrontSize), (32,gStrontSize), (24+16,gStrontSize),(24+32,gStrontSize), (24+64,gStrontSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r662"] # + 64 cores from KNL
config.nodes=1
config.walltimeH=5

batch = openfoam_loadManager_pbs_knl(config)
batch.run(False)
batch.run(True)


from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 0.100
globalBalanceP = 6
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"


######################################################################

globalStrongSize = 256
config = OpenFOAM_Config(session_path,
                         "4nodes-eth-static-hete",
                         globalSolver,
                         globalCase)

config.samples=8
config.table[P.LOAD_MANAGER] = [True]
config.table[P.COMM_GRAPH] = [True, False]
config.table[P.CPU] = [(8, globalStrongSize), (16, globalStrongSize),(32, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["staticFvMesh"]

config.nodeList=["r641", "r421", "r641", "r421"]
config.netInterface = "tcp"
config.walltimeH=1

config.mindHT = "partial"

#HOMOGENEOUS
batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)

######################################################################

globalStrongSize = 128
config = OpenFOAM_Config(session_path,
                         "4nodes-eth-dyn-hete",
                         globalSolver,
                         globalCase)

config.samples=8
config.table[P.LOAD_MANAGER] = [True]
config.table[P.COMM_GRAPH] = [True, False]
config.table[P.CPU] = [(8, globalStrongSize), (16, globalStrongSize),(32, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r421", "r641", "r421"]
config.netInterface = "tcp"
config.walltimeH=1

config.mindHT = "partial"

#HOMOGENEOUS
batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)


from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 0.100
globalBalanceP = 6
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"

######################################################################
globalStrongSize = 512

config = OpenFOAM_Config(session_path,
                         "2nodes-dyn-homog",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [False]
config.table[P.POWER_MANAGER] = [True, False]
config.table[P.POWER_CAP] = [0.85, 1]
config.table[P.CPU] = [(16, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r641"]
config.netInterface = "myri"

#batch = openfoam_loadManager_pbs_single_job(config)
#batch.run(False)
#batch.run(True)


######################################################################
globalStrongSize = 1024

config = OpenFOAM_Config(session_path,
                         "4nodes-dyn-homog",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [False]
config.table[P.POWER_MANAGER] = [True, False]
config.table[P.POWER_CAP] = [0.85, 1]
config.table[P.CPU] = [(32, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r641", "r641", "r641"]
config.netInterface = "myri"

#batch = openfoam_loadManager_pbs_single_job(config)
#batch.run(False)
#batch.run(True)

######################################################################

globalStrongSize = 2048

config = OpenFOAM_Config(session_path,
                         "8nodes-dyn-homog",
                         globalSolver,
                         globalCase)

config.samples=2
config.table[P.LOAD_MANAGER] = [False]
config.table[P.POWER_MANAGER] = [True, False]
config.table[P.POWER_CAP] = [ 0.85,1]
config.table[P.CPU] = [(64, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r641", "r641", "r641", "r641", "r641", "r641", "r641"]
config.netInterface = "myri"

#batch = openfoam_loadManager_pbs_single_job(config)
#batch.run(False)
#batch.run(True)

######################################################################

globalStrongSize = 4096

config = OpenFOAM_Config(session_path,
                         "16nodes-dyn-homog",
                         globalSolver,
                         globalCase)

config.samples=4
config.table[P.LOAD_MANAGER] = [False]
config.table[P.POWER_MANAGER] = [True, False]
config.table[P.POWER_CAP] = [1, 0.85, 0.7]
config.table[P.CPU] = [(64, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r641", "r641", "r641", "r641", "r641", "r641", "r641", "r641", "r641", "r641", "r641", "r641", "r641", "r641", "r641"]
config.netInterface = "myri"

config.mindHT = "partial"

batch = openfoam_loadManager_pbs_single_job(config)
batch.run(False)
batch.run(True)


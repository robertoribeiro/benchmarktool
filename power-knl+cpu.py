from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 0.100
globalBalanceP = 6
globalMeshType = "staticFvMesh"
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"


######################################################################


config = OpenFOAM_Config(session_path, 
                         "static-half",
                         globalSolver,
                         globalCase)

config.samples=5
config.table[P.LOAD_MANAGER] = [False]
config.table[P.POWER_MANAGER] = [True, False]
config.table[P.POWER_CAP] = [0.7,0.85, 1]
config.table[P.CPU] = [ (16+32,1024)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["staticFvMesh"]

config.nodeList=["r641"] # + 64 cores from KNL
config.nodes=1
config.walltimeH=6

#batch = openfoam_loadManager_pbs_knl(config)
#batch.run(False)
#batch.run(True)






######################################################################


config = OpenFOAM_Config(session_path, 
                         "dyn-half",
                         globalSolver,
                         globalCase)

config.samples=5
config.table[P.LOAD_MANAGER] = [False]
config.table[P.POWER_MANAGER] = [True, False]
config.table[P.POWER_CAP] = [0.7,0.85, 1]
config.table[P.CPU] = [ (16+32,1024)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641"] # + 64 cores from KNL
config.nodes=1
config.walltimeH=6

batch = openfoam_loadManager_pbs_knl(config)
batch.run(False)
batch.run(True)

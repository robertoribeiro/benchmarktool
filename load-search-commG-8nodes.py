from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 0.100
globalBalanceP = 6
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"

######################################################################
globalStrongSize = 2048

config = OpenFOAM_Config(session_path,
                         "8nodes-myri-dyn-homog",
                         globalSolver,
                         globalCase)

config.samples=5
config.table[P.LOAD_MANAGER] = [True]
config.table[P.COMM_GRAPH] = [True, False]
config.table[P.CPU] = [(8*8, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r641", "r641", "r641", "r641", "r641", "r641", "r641"]
config.netInterface = "myri"
#config.walltimeH=1.5

#config.mindHT = "partial"

#HOMOGENEOUS
#batch =  openfoam_loadManager_pbs_single_job(config)
#batch.run(False)
#batch.run(True)


######################################################################

globalStrongSize = 1024

config = OpenFOAM_Config(session_path,
                         "8nodes-eth-dyn-homog",
                         globalSolver,
                         globalCase)

config.samples=10
config.table[P.LOAD_MANAGER] = [True]
config.table[P.COMM_GRAPH] = [False, True]
config.table[P.CPU] = [(4*8, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r641", "r641", "r641", "r641", "r641", "r641", "r641"]
config.netInterface = "tcp"
#config.walltimeH=1.5

#config.mindHT = "partial"

#HOMOGENEOUS
batch =  openfoam_loadManager_pbs_single_job(config)
batch.run(False)
batch.run(True)


######################################################################


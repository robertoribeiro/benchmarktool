from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 0.070
globalBalanceP = 6
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"

globalStrongSize = 1024
######################################################################
config = OpenFOAM_Config(session_path,
                         "static-hete",
                         globalSolver,
                         globalCase)

config.samples=5
config.table[P.LOAD_MANAGER] = [True]
config.table[P.COMM_GRAPH] = [True, False]
config.table[P.CPU] = [(8, 512), (16, 512), (8, globalStrongSize), (16, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["staticFvMesh"]

config.nodeList=["r641", "r421"]
config.netInterface = "tcp"
config.walltimeH=4

batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)


######################################################################
globalStrongSize = 512

config = OpenFOAM_Config(session_path,
                         "dyn-homog",
                         globalSolver,
                         globalCase)

config.samples=5
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.COMM_GRAPH] = [True, False]
config.table[P.CPU] = [(8, 256), (16, 256), (8, globalStrongSize), (16, globalStrongSize)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r641"]
config.netInterface = "tcp"
config.walltimeH=4

batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)


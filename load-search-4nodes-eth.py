from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 0.070
globalBalanceP = 6
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"

globalStrongSize = 512
######################################################################
config = OpenFOAM_Config(session_path,
                         "static-hete",
                         globalSolver,
                         globalCase)

config.samples=10
config.table[P.LOAD_MANAGER] = [True]
config.table[P.COMM_GRAPH] = [False, True]
config.table[P.CPU] = [(4, 512), (8, 512),( 16, 512), (32, 512)]
#config.table[P.CPU] = [(4, 1024), (8, 1024),(16, 1024), (32, 1024)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["staticFvMesh"]

config.nodeList=["r641", "r421", "r641", "r421"]
config.netInterface = "tcp"
config.walltimeH=1

#batch = openfoam_loadManager_pbs_multi_node(config)
#batch.run(False)
#batch.run(True)

######################################################################

config = OpenFOAM_Config(session_path,
                         "dyn-hete-512",
                         globalSolver,
                         globalCase)

config.samples=7
config.table[P.LOAD_MANAGER] = [True]
config.table[P.COMM_GRAPH] = [False, True]
config.table[P.CPU] = [(32, 512)]
#config.table[P.CPU] = [(4, 512), (8, 512),( 16, 512), (32, 512)]
config.table[P.CPU] = config.table[P.CPU][::-1]
#config.table[P.CPU] = [(4, 1024), (8, 1024),(16, 1024), (32, 1024)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = ["dynamicRefineExtFvMesh"]

config.nodeList=["r641", "r421", "r641", "r421"]
config.netInterface = "tcp"
config.walltimeH=1

batch = openfoam_loadManager_pbs_multi_node(config)
batch.run(False)
batch.run(True)


import shutil
import os
import subprocess


CWD = os.getcwd();

path = CWD+"/paraviewParallelView_local.py"
if os.path.isfile(path):
        os.remove(path) 

shutil.copy("/home/rr/work/openfoam/run/benchmarktool/paraviewParallelView.py", path)

command = ['paraview', '--script=./paraviewParallelView_local.py']
print command
ps = subprocess.Popen(command, stdout=subprocess.PIPE)
out, err = ps.communicate()
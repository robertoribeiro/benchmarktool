import sys

# if sys.version_info[0] < 3:
#    print("This script requires Python version 3")
#    sys.exit(1)

import numpy
import itertools
import shutil
import subprocess
import time
import os
import re
import signal
import traceback
import collections

from enum import Enum, unique  # pip install --upgrade pip enum34
from collections import OrderedDict
from tabulate import tabulate
from time import sleep

from bench import BenchEntry, inplace_change, prepare_folder_structure, \
    send_pushbullet
from math import ceil

import pickle


@unique
class P(Enum):
    CPU = "CPUs"
    MESH_SIZE = "mesh-size"
    ENDTIME = "endtime"
    IMB_TOL = "imb-tol"
    BALANCE_PERIOD = "balance-period"
    MIN_PERC = "min-perc"
    ITR = "ITR"
    HETEROGENEITY = "heterogeneity"
    LOAD_MANAGER = "nSharmaLoadManager"
    POWER_MANAGER = "nSharmaPowerManager"
    MESH_TYPE = "mesh-type"
    COMM_GRAPH = "comm-graph"
    POWER_CAP = "power-cap"


class openfoam_loadManager(BenchEntry):

    def __init__(self, cfg):

        self.initial_config = cfg
        parameterMatrix = []
        parameterNames = []
        for key, value in cfg.table.items():
            if len(value) > 1:
                parameterNames.append(key)
                parameterMatrix.append(value)

        # if len(parameterNames) == 0:
        #    print("No iterative parameters specified")
        #    sys.exit(1)

        super(openfoam_loadManager, self).__init__(
            cfg.solver + "-" + cfg.entry_name, cfg.session_path,
            cfg.samples, parameterNames, parameterMatrix)

        self.session_base_case_path = self.session_path + "/_base"

        self.solver = cfg.solver
        self.cellCountMethod = cfg.cellCountMethod

        self.resultPropertyNames = ["SolvingProblem",
                                    "SolvingProblemSampleMean",
                                    "SolvingSim",
                                    "SolvingSimSampleMean",
                                    "full",
                                    "loadBalanceCells",
                                    "updateLoadManager",
                                    "parMetis",
                                    "redistribute",
                                    "Boundary_Mean",
                                    "Boundary_Nodes_Mean",
                                    "Busy_RSTD_mean%",
                                    "Idle_RSTD_mean%",
                                    "Final_#Cells",
                                    "LSolver_iterations",
                                    "Total_Idle",
                                    "Total_Busy",
                                    "Total_Idle%",
                                    "Total_Busy%",
                                    "powerUsed",
                                    "Energy"
                                    ]

        self.toPlot = []

        # results will presented in 2D tables, so in the list of parameters
        # there is a
        # point where part of the params will do rows and the other part will
        #  do columns
        break_index = int(self.number_of_parameters / 2)

        ff = []
        for p in range(break_index):
            tt = []
            for l in self.parameterMatrix[p]:
                tt.append((self.parameterNames[p], l))
            ff.append(tt)

        # get all permuatitions for the rows
        self.i_combinations = list(itertools.product(*ff))

        ff = []
        for p in range(break_index, self.number_of_parameters):
            tt = []
            for l in self.parameterMatrix[p]:
                tt.append((self.parameterNames[p], l))
            ff.append(tt)

        # get all permuatitions for the cols    
        self.j_combinations = list(itertools.product(*ff))

        # create a map between row and col combinations to 2D matrix indices
        # the keys are tuples resulted from combining all rows and col
        # combinations (p1,p2,p3,..., pN)
        # see saveProperty()
        self.tuple_to_matrix = {}
        for i in range(len(self.i_combinations)):
            for j in range(len(self.j_combinations)):
                self.tuple_to_matrix[
                    self.i_combinations[i] + self.j_combinations[j]] = (i, j)

        self.result_tree = {}
        for prop in self.resultPropertyNames:
            self.result_tree[prop] = numpy.zeros(
                (len(self.i_combinations), len(self.j_combinations)))

        self.cellCount = set([])
        self.getCells = True

        if not os.path.exists(self.session_base_case_path):
            shutil.copytree(self.initial_config.foam_case_path,
                            self.session_base_case_path)

        self.totalRuns = 0

    def run(self, post):

        if not post:
            filehandler = open(self.entry_path + "/config.obj", "wb")
            pickle.dump(self.initial_config, filehandler)
            filehandler.flush()

        start = time.time()
        super(openfoam_loadManager, self).run(post)
        end = time.time()
        self.myLog("Time: " + str(end - start) + "\n")

    # only valid for damBreak!
    def set_number_of_cells(self, v):

        if self.initial_config.solver == "interDyMFoam":
            self.getCells = True
            if self.cellCountMethod == "refine":
                if v > 0:
                    ref = ""
                    for i in range(v):
                        ref += " runApplication -l log.refineMesh" + str(
                            i + 1) + " refineMesh -overwrite;"

                        inplace_change(
                            self.current_iteration_folder + "/_base_mutated" +
                            "/Allrun", "runApplication blockMesh",
                            "runApplication blockMesh;" + ref)
            elif self.cellCountMethod == "file":
                shutil.copy(
                    self.current_iteration_folder + "/_base_mutated" +
                    self.initial_config.blockMeshDictLocation +
                    "blockMeshDict-" + str(
                        v),
                    self.current_iteration_folder + "/_base_mutated" +
                    self.initial_config.blockMeshDictLocation + "blockMeshDict")

            elif self.cellCountMethod == "analytical-damBreak":

                # grad = int(math.log(v+1, 3)*51)
                grad = int((v * 1024) ** (1. / 3))
                coef = 1.0 / 32.0 * float(grad)

                newV = "vertices\n"
                newV = newV + "(\n"
                newV = newV + "    (0 0 0)\n"
                newV = newV + "    (1 0 0)\n"
                newV = newV + "    (1 1 0)\n"
                newV = newV + "    (0 1 0)\n"
                newV = newV + "    (0 0 1)\n"
                newV = newV + "    (1 0 1)\n"
                newV = newV + "    (1 1 1)\n"
                newV = newV + "    (0 1 1)\n"
                newV = newV + ");\n"

                newV = newV.replace("1", str(coef))

                inplace_change(
                    self.current_iteration_folder + "/_base_mutated" +
                    self.initial_config.blockMeshDictLocation + "blockMeshDict",
                    "vertices.*\n\((?:\s|\n)*(?:\(\s*(?:(?:\d|\.)\s*)*\)("
                    "?:\s|\n)*)*\);",
                    newV)

                inplace_change(
                    self.current_iteration_folder + "/_base_mutated" +
                    self.initial_config.blockMeshDictLocation + "blockMeshDict",
                    "\(\s*\d*\s*\d*\s*\d*\s*\)\s*simpleGrading",
                    "(" + str(grad) + " " + str(grad) + " " + str(
                        grad) + ") simpleGrading")

                inplace_change(
                    self.current_iteration_folder +
                    "/_base_mutated/system/setFieldsDict",
                    "box\s*\((?:\s|\d|\.)*\)\s*\(((?:\s|\d|\.)*)\);",
                    "box ( 0 0 0 ) (" + str(0.6 * coef) + " " + str(
                        0.1875 * coef) + " " + str(0.75 * coef) + ");")

            if P.MESH_TYPE in self.parameterNames:
                mesh = self.getCurrentParameter(
                    self.parameterNames.index(P.MESH_TYPE))
            else:
                mesh = self.initial_config.table[P.MESH_TYPE][0]

            if mesh == "dynamicRefineExtFvMesh" or mesh == \
                    "dynamicRefineFvMesh" or mesh == \
                    "dynamicRefineBalancedFvMesh":
                inplace_change(
                    self.current_iteration_folder + "/_base_mutated" +
                    "/constant/dynamicMeshDict",
                    "maxCells\s*\d*;",
                    "maxCells   " + str(
                        v * 1024 * 7) + ";")  # ootb case is 32K -> 200K

        if self.initial_config.solver == "simpleDyMFoam":

            self.getCells = True
            if self.cellCountMethod == "analytical-buildings":
                coef = v ** (1. / 3)

                xMin = -20 * coef
                xMax = 330 * coef
                yMin = -50 * coef
                yMax = 230 * coef
                zMax = 140 * coef
                xCells = round(25 * coef, 0)
                yCells = round(20 * coef, 0)
                zCells = round(10 * coef, 0)

                newV = "backgroundMesh\n"
                newV = newV + "{\n"
                newV = newV + "    xMin   " + str(xMin) + ";\n"
                newV = newV + "    xMax   " + str(xMax) + ";\n"
                newV = newV + "    yMin   " + str(yMin) + ";\n"
                newV = newV + "    yMax   " + str(yMax) + ";\n"
                newV = newV + "    zMin     0;\n"
                newV = newV + "    zMax   " + str(zMax) + ";\n"
                newV = newV + "    xCells  " + str(int(xCells)) + ";\n"
                newV = newV + "    yCells  " + str(int(yCells)) + ";\n"
                newV = newV + "    zCells  " + str(int(zCells)) + ";\n"
                newV = newV + "}\n"

                inplace_change(
                    self.current_iteration_folder + "/_base_mutated" +
                    self.initial_config.blockMeshDictLocation + "blockMeshDict",
                    "backgroundMesh(?:\n|\s|\t)*{(?:\n|\s|\t)*(?:.*\n)*}", newV)

                inplace_change(
                    self.current_iteration_folder +
                    "/_base_mutated/system/snappyHexMeshDict",
                    "scale.*", "scale  " + str(coef) + ";")

                inplace_change(
                    self.current_iteration_folder +
                    "/_base_mutated/system/snappyHexMeshDict",
                    "max.*", "max (" + str(250 * coef) + " " + str(
                        180 * coef) + " " + str(90 * coef) + ");")

    def set_number_domains(self, n):
        if n > 1:
            inplace_change(
                self.current_iteration_folder + "/_base_mutated" + "/Allrun",
                "(runParallel|runParallel_tee|runApplication) " + self.solver
                + " *[0-9]*",
                "runParallel " + self.solver + "   %d" % n)
            inplace_change(
                self.current_iteration_folder + "/_base_mutated" +
                "/system/decomposeParDict",
                "numberOfSubdomains +[0-9]+", "numberOfSubdomains   %d" % n)

            inplace_change(
                self.current_iteration_folder + "/_base_mutated" + "/Allrun",
                "#runApplication decomposePar", "runApplication decomposePar")

            if os.path.isfile(
                    self.current_iteration_folder + "/_base_mutated" +
                    "/system/balanceParDict"):
                inplace_change(
                    self.current_iteration_folder + "/_base_mutated" +
                    "/system/balanceParDict",
                    "numberOfSubdomains +[0-9]+", "numberOfSubdomains   %d" % n)

            self.set_heterogeneityF(
                self.initial_config.table[P.HETEROGENEITY][0], n)

        else:
            inplace_change(self.current_iteration_folder + "/_base_mutated" +
                           "/Allrun",
                           "(runParallel|runParallel_tee|runApplication) " +
                           self.solver + " *[0-9]*",
                           "runApplication " + self.solver)
            inplace_change(
                self.current_iteration_folder + "/_base_mutated" + "/Allrun",
                "runApplication decomposePar", "#runApplication decomposePar")

    def set_end_time(self, n):
        inplace_change(self.current_iteration_folder + "/_base_mutated" +
                       "/system/controlDict", "endTime +[0-9]*\.*[0-9]*",
                       "endTime   %f" % n)

    def set_heterogeneityF(self, v, n):
        newH = "factors ("
        for i in range(n):
            if i % 2 == 0: newH += str(v) + " "
            if i % 2 != 0: newH += str(0) + " "

        newH += ")"
        inplace_change(self.current_iteration_folder + "/_base_mutated" +
                       "/system/nSharmaDict",
                       "factors +\((?:(?:\d*\.)?\d+ *)+\)",
                       newH)

    def set_balance_period(self, v):
        inplace_change(self.current_iteration_folder + "/_base_mutated" +
                       "/system/nSharmaDict", "balancePeriod +[0-9]*\.?[0-9]*",
                       "balancePeriod   %d" % v)

    def enable_loadManager(self, v):
        inplace_change(self.current_iteration_folder + "/_base_mutated" +
                       "/system/nSharmaDict", "enableLoadBalance +(0|1)",
                       "enableLoadBalance   %d" % v)

    def enable_powerManager(self, v):
        inplace_change(self.current_iteration_folder + "/_base_mutated" +
                       "/system/nSharmaDict", "enablePowerOptimize +(0|1)",
                       "enablePowerOptimize   %d" % v)

    def enable_commGraph(self, v):
        inplace_change(self.current_iteration_folder + "/_base_mutated" +
                       "/system/nSharmaDict", "useCommGraphPartitioning +(0|1)",
                       "useCommGraphPartitioning   %d" % v)

    def set_mesh(self, mesh):
        inplace_change(self.current_iteration_folder + "/_base_mutated" +
                       "/constant/dynamicMeshDict",
                       "dynamicFvMesh\s+("
                       "staticFvMesh|dynamicRefineExtFvMesh"
                       "|dynamicRefineFvMesh|dynamicRefineBalancedFvMesh)",
                       "dynamicFvMesh   " + mesh)

    def set_min_percentage(self, v):
        inplace_change(self.current_iteration_folder + "/_base_mutated" +
                       "/system/nSharmaDict",
                       "minPercentageCellsMoved +[0-9]*\.?[0-9]*",
                       "minPercentageCellsMoved   %f" % v)

    def set_metis_imbalance_tol(self, v):
        inplace_change(self.current_iteration_folder + "/_base_mutated" +
                       "/system/nSharmaDict",
                       "parmetisImbalanceTolerance +[0-9]*\.?[0-9]*",
                       "parmetisImbalanceTolerance   %f" % v)

    def set_power_cap(self, v):
        inplace_change(self.current_iteration_folder + "/_base_mutated" +
                       "/system/nSharmaDict", "powerCap +[0-9]*\.?[0-9]*",
                       "powerCap   %f" % v)

    def set_metis_ITR(self, v):
        inplace_change(self.current_iteration_folder + "/_base_mutated" +
                       "/system/nSharmaDict", "parmetisITR +[0-9]*\.?[0-9]*",
                       "parmetisITR   %f" % v)

    def checkGracefullExecute(self):
        repeatSample = False
        try:
            sample_folder = self.current_iteration_folder + "/sample" + str(
                self.sample_number)
            fh = open(sample_folder + "/log." + self.solver)
            if not "End\n" in fh.read():
                self.failCount += 1
                repeatSample = True
                if self.failCount > self.initial_config.failCount:
                    repeatSample = False
                    self.failCount = 0
                    raise Exception("Max number of run failures achieved")

                raise Exception("End, not found")

        except Exception as e:
            self.myLog("checkGracefullExecute error: " + str(e))
            self.myLog(sample_folder)

            if self.initial_config.pushbullet:
                send_pushbullet(
                    os.path.basename(self.session_path) + "-" + self.name,
                    "Run " + sample_folder + " failed!\n" + str(e))

            print(traceback.format_exc())

        return repeatSample

    def set_defaults(self):
        for key, value in self.initial_config.table.items():
            if key == P.ENDTIME:
                self.set_end_time(value[0])
            elif key == P.IMB_TOL:
                self.set_metis_imbalance_tol(value[0])
            elif key == P.CPU:

                if (len(value[0]) > 1):
                    self.set_number_domains(value[0][0])
                    self.set_number_of_cells(value[0][1])

                    if value[0][0] == 1:
                        self.enable_loadManager(False)
                        self.enable_powerManager(False)
                else:
                    self.set_number_domains(value[0])

            elif key == P.MESH_SIZE:
                self.set_number_of_cells(value[0])
            elif key == P.BALANCE_PERIOD:
                self.set_balance_period(value[0])
            elif key == P.MIN_PERC:
                self.set_min_percentage(value[0])
            elif key == P.ITR:
                self.set_metis_ITR(value[0])
            elif key == P.LOAD_MANAGER:
                self.enable_loadManager(value[0])
            elif key == P.POWER_MANAGER:
                self.enable_powerManager(value[0])
            elif key == P.COMM_GRAPH:
                self.enable_commGraph(value[0])
            elif key == P.MESH_TYPE:
                self.set_mesh(value[0])
            elif key == P.POWER_CAP:
                self.set_power_cap(value[0])
            elif key == P.HETEROGENEITY:
                self.set_heterogeneityF(value[0],
                                        self.initial_config.table[P.CPU][0][0])
            else:
                print("Unknown parameter")

    def mutate(self):
        shutil.copytree(self.session_base_case_path,
                        self.current_iteration_folder + "/_base_mutated")

        # ensure specified and intial parameter values are applied
        self.set_defaults()

        for i in range(self.number_of_parameters):
            if self.parameterNames[i] == P.ENDTIME:
                self.set_end_time(self.getCurrentParameter(i))
            elif self.parameterNames[i] == P.IMB_TOL:
                self.set_metis_imbalance_tol(self.getCurrentParameter(i))
            elif self.parameterNames[i] == P.CPU:

                v = self.getCurrentParameter(i);
                if (len(v) > 1):
                    self.set_number_domains(v[0])
                    self.set_number_of_cells(v[1])
                    if v == 1:
                        self.enable_loadManager(False)
                        self.enable_powerManager(False)
                else:
                    self.set_number_domains(v[0])

            elif self.parameterNames[i] == P.MESH_SIZE:
                self.set_number_of_cells(self.getCurrentParameter(i))
            elif self.parameterNames[i] == P.BALANCE_PERIOD:
                self.set_balance_period(self.getCurrentParameter(i))
            elif self.parameterNames[i] == P.POWER_CAP:
                self.set_power_cap(self.getCurrentParameter(i))
            elif self.parameterNames[i] == P.MIN_PERC:
                self.set_min_percentage(self.getCurrentParameter(i))
            elif self.parameterNames[i] == P.ITR:
                self.set_metis_ITR(self.getCurrentParameter(i))
            elif self.parameterNames[i] == P.LOAD_MANAGER:
                self.enable_loadManager(self.getCurrentParameter(i))
                if P.CPU in self.parameterNames:
                    cpus = \
                    self.getCurrentParameter(self.parameterNames.index(P.CPU))[
                        0]
                else:
                    cpus = self.initial_config.table[P.CPU][0][0]
                if cpus == 1:
                    self.enable_loadManager(False)
            elif self.parameterNames[i] == P.POWER_MANAGER:
                self.enable_powerManager(self.getCurrentParameter(i))
                if P.CPU in self.parameterNames:
                    cpus = \
                    self.getCurrentParameter(self.parameterNames.index(P.CPU))[
                        0]
                else:
                    cpus = self.initial_config.table[P.CPU][0][0]
                if cpus == 1:
                    self.enable_powerManager(False)
            elif self.parameterNames[i] == P.MESH_TYPE:
                self.set_mesh(self.getCurrentParameter(i))
            elif self.parameterNames[i] == P.COMM_GRAPH:
                self.enable_commGraph(self.getCurrentParameter(i))
            elif self.parameterNames[i] == P.HETEROGENEITY:
                if P.CPU in self.parameterNames:
                    cpus = \
                    self.getCurrentParameter(self.parameterNames.index(P.CPU))[
                        0]
                else:
                    cpus = self.initial_config.table[P.CPU][0][0]
                self.set_heterogeneityF(self.getCurrentParameter(i), cpus)
            else:
                print("Unknown parameter")

    def pre_finalize_iteration(self):
        try:

            out, sample = self.getSolvingTime("SolvingProblem")
            # self.saveTime(out)
            self.saveProperty("SolvingProblem", out)
            self.saveProperty("SolvingProblemSampleMean",
                              self.getSolvingTimeMean("SolvingProblem"))
            self.saveProperty("SolvingSimSampleMean",
                              self.getSolvingTimeMean("SolvingSim"))

            try:
                self.loadManagerDataPostProcessing(sample)
            except Exception as e:
                print "loadManagerDataPostProcessing: " + str(e)
                print(traceback.format_exc())

            if self.getCells:
                s = open(self.current_iteration_folder + "/sample" + str(
                    sample) + "/log.checkMesh").read()
                cells = re.findall("cells: *[0-9]+", s)
                self.cellCount.add(int(cells[0].split()[1]))
                self.getCells = False

            # l = self.pretty_print()    
            # if self.initial_config.pushbullet:
            #       send_pushbullet(os.path.basename(self.session_path) + "-"
            # + self.name + " iteration done",l )

        except Exception as e:
            print "pre_finalize_iteration error: " + str(e)
            print(traceback.format_exc())

    def post_finalize_iteration(self):
        pass

    # def saveTime(self, value):

    #         x = self.current_parameter_index[1] * len(self.parameterMatrix[
    # 0]) + self.current_parameter_index[0]
    #         y = self.current_parameter_index[3] * len(self.parameterMatrix[
    # 2]) + self.current_parameter_index[2]

    #         x = self.second_field_index * len(self.first_field) +
    # self.first_field_index
    #         y = self.fourth_field_index * len(self.third_field) +
    # self.third_field_index

    # self.best_times[y][x] = value

    def saveProperty(self, key, value):

        # create the key or the current paramter values and get the 2D matrix
        #  positions to store the result
        map_key = ()
        for param in range(self.number_of_parameters):
            map_key = map_key + ((self.parameterNames[param],
                                  self.parameterMatrix[param][
                                      self.current_parameter_index[param]]),)

        self.result_tree[key][self.tuple_to_matrix[map_key][0]][
            self.tuple_to_matrix[map_key][1]] = float("{0:.3f}".format(value))

    #     def getExecutionTime(self):
    #         chosen_sample =0
    #         min_v=sys.float_info.max
    #         for x in range(self.max_samples):
    #             sample_folder = self.current_iteration_folder + "/sample" +
    #  str(x)
    #             with open(sample_folder + "/logs/executionTime_0") as fh:
    #                 for line in fh:
    #                     pass
    #                 last = line
    #             v= float(last.split()[1])
    #             if v < min_v:
    #                 min_v = v
    #                 chosen_sample=x
    #
    #         return min_v, chosen_sample

    def getCurrentIterationParameterValue(self, param):

        if param in self.parameterNames:
            v = self.getCurrentParameter(self.parameterNames.index(param))
        else:
            v = self.initial_config.table[param][0]

        if (isinstance(v, tuple)):
            return v[0]
        else:
            return v

    def getSolvingTime(self, prop):

        chosen_sample = 0
        min_v = sys.float_info.max
        for x in range(self.max_samples):
            sample_folder = self.current_iteration_folder + "/sample" + str(x)
            line = str(sys.float_info.max)
            if os.path.exists(sample_folder + "/log." + self.solver):
                with open(sample_folder + "/log." + self.solver) as fh:
                    data = fh.read().split("Finishing load manager")
                    if len(data) > 1:
                        line = \
                        re.search(prop + " *[0-9]*\.*[0-9]*", data[1]).group(
                            0).split()[1]

            v = float(line)
            if v < min_v:
                min_v = v
                chosen_sample = x

        return min_v, chosen_sample

    def getSolvingTimeMean(self, prop):

        times = []
        for x in range(self.max_samples):
            sample_folder = self.current_iteration_folder + "/sample" + str(x)
            line = str(sys.float_info.max)
            if os.path.exists(sample_folder + "/log." + self.solver):
                with open(sample_folder + "/log." + self.solver) as fh:
                    data = fh.read().split("Finishing load manager")
                    if len(data) > 1:
                        line = \
                        re.search(prop + " *[0-9]*\.*[0-9]*", data[1]).group(
                            0).split()[1]
                        v = float(line)
                        times.append(v)

        times.sort()

        return numpy.average(times[:3])

    def pretty_print(self):

        # array of tuples, one for each line composing the row header
        r = []
        for t in self.i_combinations:
            rt = ()
            for v in t:
                rt = rt + (str(v[0].value) + "." + str(v[1]),)
            r = r + [rt]

        # for i in range(len(r)):
        #    r[i] = r[i] + ("*",)

        # arrays of arrays composing column header
        h = []
        for p in range(len(self.j_combinations[0])):
            rt = [" "] * len(r[0])
            for t in self.j_combinations:
                rt.append(str(t[p][0].value) + "." + str(t[p][1]))
            h.append(rt)

        # h.append(["*"] * len(h[0]))
        # h = h[::-1]

        for i in range(len(r[0])):
            h[0][i] = " "

        result = os.path.basename(os.getcwd()) + "\n"
        result += self.name + "\n"
        result += tabulate([["cells"] + list(self.cellCount)],
                           tablefmt="orgtbl") + "\n\n"

        for prop in self.resultPropertyNames:
            result += prop + "\n"
            # header columns
            first_column = numpy.array(r)
            re = self.result_tree[prop].tolist()
            # convert arrays of float to array of str
            re = [[str(t) for t in x] for x in re]
            ab = numpy.hstack((first_column, re))

            Tid = -1

            if P.LOAD_MANAGER in self.initial_config.table:
                if self.initial_config.table[P.LOAD_MANAGER] == [True, False]:
                    Tid = 0
                else:
                    if self.initial_config.table[P.LOAD_MANAGER] == [False,
                                                                     True]:
                        Tid = 1

            if Tid != -1:
                if prop == "SolvingProblem" or \
                        prop == "full" or \
                        prop == "SolvingProblemSampleMean" or \
                        prop == "SolvingSimSampleMean" or \
                        prop == "SolvingSim":
                    try:
                        Fid = 0
                        if Tid == 0: Fid = 1
                        gain = self.result_tree[prop][Fid] / \
                               self.result_tree[prop][Tid]
                        gain = numpy.round(gain, 2)
                        gain = [str(l) for l in gain]
                        # appe = [numpy.append( ["nSharma gain","*"], gain)]
                        appe = [numpy.append(["nSharma gain"], gain)]
                        ab = numpy.vstack((ab, appe))
                    except Exception as e:
                        pass

                if prop == "Final_#Cells" or \
                        prop == "LSolver_iterations":
                    try:
                        Fid = 0
                        if Tid == 0: Fid = 1
                        gain = self.result_tree[prop][Tid] * 1.0 / \
                               self.result_tree[prop][Fid] * 1.0
                        gain = numpy.round(gain - 1, 2)
                        # appe = [numpy.append( ["nSharma gain","*"], gain)]
                        gain = [str(l) for l in gain]
                        appe = [numpy.append(["%"], gain)]
                        ab = numpy.vstack((ab, appe))
                    except Exception as e:
                        pass

            for k in range(len(h)):
                ab = numpy.vstack((h[k], ab))

            if len(ab) <= 3:
                ab = [["*"] * len(ab)] + ab.tolist()

            result += tabulate(ab, tablefmt="orgtbl") + "\n\n"

        self.myLog(result)
        return result

    def profileLogGetProp(self, prop, log):
        r = []
        last = []
        for a in log:
            r += re.findall("(" + prop + " +(?:(?:(?:\d*\.)?\d+)| +)+)", a)

        if len(r) > 0:
            last = r[-1]
            last = last.split()
            last.pop(0)
            # last = map(float, last)
            last = [float(i) for i in last]

        table = re.sub(" +", ",", "\n".join(r))
        return table + "\n", last

    def profileLogGetTotalSolverIterations(self, log):
        r = []

        r += re.findall("No Iterations (\d*)", log)

        totalIts = 0
        for its in r:
            totalIts += int(its)

        return totalIts

    def profileLogGetPropDiffPerLine(self, prop, log):
        r = []
        last = []
        for a in log:
            r += re.findall("(" + prop + " +(?:(?:(?:\d*\.)?\d+)| +)+)", a)

        if len(r) > 0:
            last = r[-1]
            last = last.split()
            last.pop(0)
            # last = map(float, last)
            last = [float(i) for i in last]

        table = re.sub(" +", ",", "\n".join(r))

        t = 0
        tt = 0
        times = []
        lines = table.split()
        for i in range(len(lines)):
            l = lines[i]
            tt = float(l.split(",")[1])
            times.append(str(tt - t))
            t = tt

        last = times[-1]

        a = "\n" + prop + "-per-episode,"
        table = a.join(times)

        return "\n" + prop + "-per-episode," + table + "\n\n", last

    def profileLogGetWeights(self, log, key):
        r = []
        for a in log:
            r += re.findall(
                key + " +\d+(?:\{|\()((?:(?:\d*\.)?\d+| )+)(?:\}|\))", a)
            # r += re.findall(key + " +\d+\(((?:(?:\d*\.)?\d+| )+)\)", a)

        a = '\n' + key + ' '
        table = re.sub(" +", ",", key + " " + a.join(r))

        return table + "\n"

    def profileLogGetBEPchanges(self, log):
        r = []
        for a in log:
            tt = re.findall("Updated balance period from (\d+) to (\d+)", a)
            if len(tt) > 0:
                r += [tt[0][0] + "," + tt[0][1]]

        a = '\n' + "balance-period-update" + ' '

        table = re.sub(" +", ",", "balance-period-update" + " " + a.join(r))

        return table + "\n"

    def profileLogGetBoundaryChanges(self, log):
        r = []
        final = 0
        initial = 0
        for a in log:
            tt = re.findall("System boundary cells:\s*(\d*)\s*->\s*(\d*)", a)
            if len(tt) > 0:
                if len(r) == 0: initial = int(tt[0][0])
                r += [tt[0][0] + "," + tt[0][1]]
                final = int(tt[0][1])

        a = '\n' + "system-boundary-cells" + ' '

        table = re.sub(" +", ",", "system-boundary-cells" + " " + a.join(r))

        pr = final
        if initial > 0:
            pr = ((final - initial) * 1.0 / initial) * 100

        return table + "\n", pr

    def profileLogGetDistributions(self, log):
        r = []
        for a in log:
            # ww = re.findall("((?:Wanted_distribution +\d+\((?:(?:(
            # ?:\d*\.)?\d+| )+)\))|No_balance_needed)", a)
            ww = re.findall(
                "Balance-episode-\d*-in-iteration-\d* ("
                "?:Distributing...|No_balance_needed)",
                a)
            if len(ww) > 1:
                ir = []
                for b in ww:
                    ir += re.findall(
                        "Wanted_distribution +\d+\(((?:(?:\d*\.)?\d+| )+)\)", b)
                r += ["   ".join(ir)]
            else:
                r += ww
        table = re.sub(" +", ",", "\n".join(r))
        return table + "\n"

    def profileLogGetPower(self, log):
        res = ""
        tres = ""
        sumAssigned = 0
        sumAvail = 0
        for a in log:
            ww = re.findall(
                "powerManager result: W: ((?:\d+ *)+) , Sum: (\d+), Wcap: ("
                "\d+)",
                a)
            if len(ww) > 0:
                res_ = "powerAssign," + ww[0][0]
                res += res_.replace(" ", ",") + "\n"

                tres += "totalPowerAssign," + ww[0][1] + "," + ww[0][2] + "\n"
                sumAssigned += int(ww[0][1])
                sumAvail += int(ww[0][2])

        return res, tres, sumAssigned, sumAvail

    def profileLogGetEnergy(self, solvingProblemPerEpisode,
                            assignedPowerPerEpisode):

        solvingProblemPerEpisode = filter(None, solvingProblemPerEpisode)
        solvingProblemPerEpisode = solvingProblemPerEpisode[:-1]
        assignedPowerPerEpisode = filter(None, assignedPowerPerEpisode)

        sum = 0
        for i in xrange(len(solvingProblemPerEpisode)):
            time = float(solvingProblemPerEpisode[i].split(",")[1])
            power = float(assignedPowerPerEpisode[i].split(",")[1])
            sum = sum + time * power
        return sum

    def profileLogProcessNumerOfCells(self, list_values):

        breaked = list_values.split()
        totalCells = ""
        RSD = ""
        table = []
        for l in breaked:
            l = l.split(",")
            l.pop(0)
            l = [int(x) for x in l]
            table.append(l)
            totalCells += "Total_Number_of_cells," + str(sum(l)) + "\n"
            v = (100 * numpy.std(numpy.array(l), ddof=1) / numpy.mean(
                numpy.array(l))).tolist()
            RSD += "Number_of_cells_RSD(%)," + str(round(v, 2)) + "\n"
            last = sum(l)

        return totalCells + "\n" + RSD, int(last)

    def loadManagerDataPostProcessing(self, sample):
        sample_folder = self.current_iteration_folder + "/sample" + str(sample)

        if not os.path.exists(sample_folder):
            return

        with open(sample_folder + "/log." + self.solver) as fh:
            data = fh.read()
        data = re.findall(
            "(\/\/\*+ LOAD_MANAGER \*+\/\/(?:\n|.)*?\/\/\*+ END-LOAD_MANAGER "
            "\*+\/\/)",
            data)
        # relist='\n'.join(relist)

        outs = []

        outs.append("sample," + str(sample) + "\n\n")

        list_values, last = self.profileLogGetProp("SolvingProblem", data)
        outs.append(list_values + "\n")

        list_values, last = self.profileLogGetPropDiffPerLine("SolvingProblem",
                                                              data)
        outs.append(list_values + "\n")

        solvingProblemPerEpisode = list_values.split("\n")

        list_values, last = self.profileLogGetProp("full", data)
        self.saveProperty("full", 0 if len(last) < 1 else numpy.mean(last))
        outs.append(list_values + "\n")

        #         list_values, last = self.profileLogGetProp("op_b:AmulCore",
        #  data)
        #         outs.append(list_values + "\n")
        #
        #         list_values, last = self.profileLogGetProp("op_b:Prec", data)
        #         outs.append(list_values + "\n")

        if self.getCurrentIterationParameterValue(P.LOAD_MANAGER):
            list_values, last = self.profileLogGetProp("loadBalanceCells", data)
            self.saveProperty("loadBalanceCells",
                              0 if len(last) < 1 else numpy.mean(last))
            outs.append(list_values + "\n")

            list_values, last = self.profileLogGetProp("updateLoadManager",
                                                       data)
            self.saveProperty("updateLoadManager",
                              0 if len(last) < 1 else numpy.mean(last))
            outs.append(list_values + "\n")

            list_values, last = self.profileLogGetProp("parMetis", data)
            self.saveProperty("parMetis",
                              0 if len(last) < 1 else numpy.mean(last))
            outs.append(list_values + "\n")

            list_values, last = self.profileLogGetProp(" redistribute", data)
            self.saveProperty("redistribute",
                              0 if len(last) < 1 else numpy.mean(last))
            outs.append(list_values + "\n")

        list_values, last = self.profileLogGetProp("SolvingSim", data)
        solvingSimTime = numpy.mean(last)
        self.saveProperty("SolvingSim", 0 if len(last) < 1 else solvingSimTime)
        outs.append(list_values + "\n")

        list_values, last = self.profileLogGetProp("Busy_time", data)
        outs.append(list_values + "\n")

        list_values, last = self.profileLogGetProp("Idle_time", data)
        outs.append(list_values + "\n")

        list_values, last = self.profileLogGetProp("Busy\(%\)", data)
        outs.append(list_values + "\n")

        list_values, last = self.profileLogGetProp("Idle\(%\)", data)
        outs.append(list_values + "\n")

        list_values, last_min = self.profileLogGetProp("Total_Idle", data)
        TotalIdleMax = numpy.max(last_min)
        self.saveProperty("Total_Idle", 0 if len(last) < 1 else TotalIdleMax)
        outs.append(list_values + "\n")

        list_values, last = self.profileLogGetProp("Total_Busy", data)
        TotalBusy = last[numpy.argmax(last_min)]
        self.saveProperty("Total_Busy", 0 if len(last) < 1 else TotalBusy)
        outs.append(list_values + "\n")

        list_values, list_values2, pwAssigned, pwAvail = \
            self.profileLogGetPower(
            data)
        outs.append(list_values + "\n")
        outs.append(list_values2 + "\n")
        assignedPowerPerEpisode = list_values2.split("\n")

        self.saveProperty("powerUsed", pwAssigned)

        energy = self.profileLogGetEnergy(solvingProblemPerEpisode,
                                          assignedPowerPerEpisode)
        self.saveProperty("Energy", energy)

        self.saveProperty("Total_Idle%", TotalIdleMax / solvingSimTime * 100)
        # outs.append(list_values + "\n")

        self.saveProperty("Total_Busy%", TotalBusy / solvingSimTime * 100)
        # outs.append(list_values + "\n")

        list_values, last = self.profileLogGetProp("Number_of_cells", data)
        outs.append(list_values + "\n")

        list_values, last = self.profileLogProcessNumerOfCells(list_values)
        self.saveProperty("Final_#Cells", last)
        outs.append(list_values + "\n")

        list_values, last = self.profileLogGetProp("Busy_Relative_STD\(%\)",
                                                   data)
        self.saveProperty("Busy_RSTD_mean%", numpy.mean(
            [float(l.split(",")[1]) for l in list_values.split()]))
        outs.append(list_values + "\n")

        list_values, last = self.profileLogGetProp("Idle_Relative_STD\(%\)",
                                                   data)
        self.saveProperty("Idle_RSTD_mean%", numpy.mean(
            [float(l.split(",")[1]) for l in list_values.split()]))
        outs.append(list_values + "\n")

        list_values, last = self.profileLogGetProp("lastRedistributeTime", data)
        outs.append(list_values + "\n")

        list_values, last = self.profileLogGetProp(
            "system-totalcellsTransfered", data)
        outs.append(list_values + "\n")

        list_values = self.profileLogGetBEPchanges(data)
        outs.append(list_values + "\n")

        its = self.profileLogGetTotalSolverIterations(
            open(sample_folder + "/log." + self.solver).read())
        self.saveProperty("LSolver_iterations", its)

        list_values, last = self.profileLogGetProp("System_Proc_BFaces", data)
        arr = [float(l.split(",")[1]) for l in list_values.split()]
        pr = arr[-1]
        # if arr[0] > 0:
        #    pr = ((arr[-1]-arr[0])*1.0/arr[0])*100
        self.saveProperty("Boundary_Mean", numpy.mean(pr))
        outs.append(list_values + "\n")

        list_values, last = self.profileLogGetProp("System_Proc_BFaces_Nodes",
                                                   data)
        arr = [float(l.split(",")[1]) for l in list_values.split()]
        pr = arr[-1]
        # if arr[0] > 0:
        #    pr = ((arr[-1]-arr[0])*1.0/arr[0])*100
        self.saveProperty("Boundary_Nodes_Mean", numpy.mean(pr))
        outs.append(list_values + "\n")

        if self.getCurrentIterationParameterValue(P.LOAD_MANAGER):
            outs.append(self.profileLogGetWeights(data, "raw-weights") + "\n")
            outs.append(
                self.profileLogGetWeights(data, "previous-weights") + "\n")
            outs.append(self.profileLogGetWeights(data,
                                                  "averaged-submitted-weights") + "\n")
            outs.append(self.profileLogGetDistributions(data) + "\n")

        # with open(self.current_iteration_folder + "/" +
        # self.get_iteration_str() + "-results.csv", 'wb') as file_csv:
        #    writer = csv.writer(file_csv,
        #                     quoting=csv.QUOTE_MINIMAL)
        #    for out in outs:    
        #        writer.writerows(out)

        with open(
                self.current_iteration_folder + "/" + self.get_iteration_str(

                ) + "-results.txt",
                'w') as file_txt:
            for out in outs:
                file_txt.write(out)

    def deleteSampleFolder(self, folder):

        while os.path.exists(folder):
            ps = subprocess.Popen(['rm', '-rf', folder], stdout=subprocess.PIPE)
            self.myLog(ps.communicate())
            sleep(10)

    def blacklist(self, nodes):

        blacklistFile = open("./blacklistFile", 'r').read().split()
        print "BlackList: "
        print blacklistFile
        print "----\n"
        for node in blacklistFile:
            if node in nodes:
                print node + " in " + nodes
                return True
        return False

    def execute(self, working_directory):

        repeatSample = True
        while repeatSample:

            shutil.copytree(self.current_iteration_folder + "/_base_mutated",
                            working_directory)
            command = working_directory + "/Allclean"
            p = subprocess.Popen(command, shell=False, cwd=working_directory,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (stdoutdata, stderrdata) = p.communicate()

            self.myLog(stdoutdata)
            self.myLog(stderrdata)

            command = working_directory + "/Allrun"
            p = subprocess.Popen(command, shell=False, cwd=working_directory,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)

            self.myLog("Running " + command)

            log = working_directory + "/log." + self.solver
            attempts = 10  # check for log file attempts

            while attempts > 0 and p.poll() == None:
                if os.path.isfile(log):
                    # fh = open(log)
                    lastTime = 0  # last updated time
                    trigger = 0  # number of time check until kill, trigger *
                    #  sleep is the time we wait basically
                    while p.poll() == None:
                        sleep(60)
                        # subprocess.call(["tail", "-n 2", log])
                        currentTime = os.path.getmtime(log)
                        self.myLog("last modified: " + time.ctime(currentTime))
                        if lastTime == currentTime:
                            trigger += 1
                            self.myLog("Process seems stalled " + str(trigger))
                        else:
                            trigger = 0
                        if trigger > 15:
                            self.myLog("Process stalled!")
                            ps = subprocess.Popen(['ps', '-ax'],
                                                  stdout=subprocess.PIPE)
                            out, err = ps.communicate()
                            for line in out.splitlines():
                                if self.solver in str(line.decode("utf-8")):
                                    pid = int(line.split(None, 1)[0])
                                    os.kill(pid, signal.SIGKILL)

                            p.terminate()
                            p.kill()
                        lastTime = currentTime

                    break
                attempts = attempts - 1
                sleep(2)

            (stdoutdata, stderrdata) = p.communicate()

            self.myLog(stdoutdata)
            self.myLog(stderrdata)

            repeatSample = self.checkGracefullExecute()
            if repeatSample:
                shutil.copytree(working_directory,
                                self.current_iteration_folder + "/failed/" +
                                self.get_sample_str() + str(
                                    int(round(time.time() * 1000))))
                self.deleteSampleFolder(working_directory)


class openfoam_loadManager_sbatch(openfoam_loadManager):

    def mutate(self):
        super(openfoam_loadManager_sbatch, self).mutate()

        cpus = self.getCurrentIterationParameterValue(P.CPU)

        nodes = self.initial_config.nodes;

        jobname = self.name + "-" + self.get_iteration_str()

        sbatch = "#!/bin/bash" + "\n" + "\n"
        sbatch = sbatch + "#SBATCH -J " + jobname + "\n"
        sbatch = sbatch + "#SBATCH -o " + jobname + ".out" + "\n"
        sbatch = sbatch + "#SBATCH -e " + jobname + ".err" + "\n"
        sbatch = sbatch + "#SBATCH -p " + self.initial_config.queue + "\n"
        sbatch = sbatch + "#SBATCH -N " + str(nodes) + "\n"
        sbatch = sbatch + "#SBATCH -n " + str(
            int(ceil(cpus / (nodes * 1.0)) * nodes)) + "\n"
        sbatch = sbatch + "#SBATCH -t 10:00:00" + "\n"
        sbatch = sbatch + "#SBATCH -A A-ccvis" + "\n"
        sbatch = sbatch + "\n"

        if self.initial_config.bynode: sbatch = sbatch + "export " \
                                                         "SLURM_TASKS_PER_NODE=\"1(x" + str(
            self.initial_config.nodes) + ")\"\n"

        sbatch = sbatch + "cdd=$PWD" + "\n"
        sbatch = sbatch + "cd $SCRATCH" + "\n"
        sbatch = sbatch + "cp -r --parents $cdd ." + "\n"
        sbatch = sbatch + "cd $PWD$cdd" + "\n"

        inplace_change(
            self.current_iteration_folder + "/_base_mutated" + "/Allrun",
            "\#\!\/bin\/(?:sh|bash)", sbatch)

        with open(self.current_iteration_folder + "/_base_mutated" + "/Allrun",
                  'a') as f:
            f.write("cdss=$PWD\n")
            f.write("cd $cdd\n")
            f.write("cd ..\n")
            f.write("cp -r $cdss .\n")

    def jobRunning(self, jobID):
        ps = subprocess.Popen(['scontrol', 'show', 'jobid', '-dd', str(jobID)],
                              stdout=subprocess.PIPE)
        out = ps.communicate()

        return "JobState=RUNNING" in str(out)

    def execute(self, working_directory):
        shutil.copytree(self.current_iteration_folder + "/_base_mutated",
                        working_directory)

        jobname = self.name + "-" + self.get_iteration_str() + "-" + \
                  self.get_sample_str()
        inplace_change(working_directory + "/Allrun", "#SBATCH -J .*",
                       "#SBATCH -J " + jobname + "\n")
        inplace_change(working_directory + "/Allrun", "#SBATCH -o .*",
                       "#SBATCH -o " + jobname + ".out" + "\n")
        inplace_change(working_directory + "/Allrun", "#SBATCH -e .*",
                       "#SBATCH -e " + jobname + ".err" + "\n")

        command = working_directory + "/Allclean"
        p = subprocess.Popen(command, shell=False, cwd=working_directory,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (stdoutdata, stderrdata) = p.communicate()

        self.myLog(stdoutdata)
        self.myLog(stderrdata)

        self.myLog(open(working_directory + "/Allrun").read())
        command = ["sbatch", working_directory + "/Allrun"]

        p = subprocess.Popen(command, shell=False, cwd=working_directory,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        (stdoutdata, stderrdata) = p.communicate()

        self.myLog(' '.join(command))

        jobID = re.findall("Submitted batch job (\d*)", str(stdoutdata))
        self.myLog(jobID)

        # if (jobID != []):

        #   jobID = jobID[0]

        # log = working_directory + "/log." + self.solver
        # attempts = 1000000  # check for log file attempts

        # while attempts > 0 or self.jobRunning(jobID):
        # if os.path.isfile(log):
        #   lastTime = 0  # last updated time
        #   trigger = 0  # number of time check until kill, trigger * sleep
        # is the time we wait basically
        #   while self.jobRunning(jobID):
        #       sleep(60)
        #       currentTime = os.path.getmtime(log)
        #                                 self.myLog("last modified:" +
        # time.ctime(currentTime))
        #                                 if lastTime == currentTime:
        #                                     trigger += 1
        #                                     self.myLog("Process seems
        # stalled " + str(trigger))
        #                                 else:
        #                                     trigger=0
        #                                 if trigger > 15:
        #                                     self.myLog("Process stalled!")
        #                                     ps = subprocess.Popen([
        # 'scancel', jobID], stdout=subprocess.PIPE)
        #                                     ps.communicate()
        #
        #                                 lastTime = currentTime
        #
        #                             break
        #                         attempts = attempts - 1
        #                         sleep(10)
        #

        # self.myLog(stdoutdata)
        # self.myLog(stderrdata)

        self.totalRuns += 1
        print "Runs: " + str(self.totalRuns)


class openfoam_loadManager_pbs(openfoam_loadManager):

    def netIface(self, default):
        m = "--mca mtl mx --mca pml cm --mca btl_tcp_if_exclude lo,eth0,eth1"
        m_simple = "--mca mtl mx --mca btl_tcp_if_exclude lo,eth0,eth1"
        t = "--mca mtl ^mx --mca btl tcp,self"

        if self.initial_config.netInterface == "myri":
            return m

        if self.initial_config.netInterface == "tcp":
            return t

        if self.initial_config.netInterface == "myri_simple":
            return m_simple

        if self.initial_config.netInterface == "":
            if default == "myri":
                return m

            if default == "tcp":
                return t


class openfoam_loadManager_pbs_knl(openfoam_loadManager_pbs):

    def mutate(self):
        super(openfoam_loadManager_pbs_knl, self).mutate()

        jobname = self.name + "-" + self.get_iteration_str()

        cpus = self.getCurrentIterationParameterValue(P.CPU)

        # ISSUE if partial or none HT mode requested
        ppn = int(ceil(cpus / 2.0));

        pbs = "#!/bin/bash" + "\n" + "\n"
        pbs = pbs + "#PBS -N " + jobname + "\n"
        pbs = pbs + "#PBS -o " + jobname + ".out" + "\n"

        if len(self.initial_config.nodeList) > 0:

            if ppn > int(self.initial_config.ppn[
                             self.initial_config.nodeList[0]] / 2):
                ppn = int(self.initial_config.ppn[
                              self.initial_config.nodeList[0]] / 2)

            if "partial" in self.initial_config.mindHT:
                request_ppn = ppn + int(self.initial_config.ppn[
                                            self.initial_config.nodeList[
                                                0]] / 2)

            if "none" in self.initial_config.mindHT:
                request_ppn = ppn

            if "full" in self.initial_config.mindHT:
                request_ppn = int(
                    self.initial_config.ppn[self.initial_config.nodeList[0]])

        if len(self.initial_config.nodeList) > 0:
            pbs = pbs + "#PBS -l nodes=" + str(
                self.initial_config.nodes) + ":ppn=" + str(request_ppn) + ":" \
                  + \
                  self.initial_config.nodeList[0] + "\n"
        else:
            pbs = pbs + "#PBS -l nodes=1:ppn=1:r641\n"

        pbs = pbs + "#PBS -l walltime=23:00:00" + "\n"
        pbs = pbs + "#PBS -V" + "\n"
        pbs = pbs + "\n"
        pbs = pbs + "source load_build 2.4.0 dev Opt > /dev/null" + "\n"
        pbs = pbs + "<cd>" + "\n"

        inplace_change(
            self.current_iteration_folder + "/_base_mutated" + "/Allrun",
            "\#\!\/bin\/(?:sh|bash)", pbs)

    def jobRunning(self, jobID):

        ps = subprocess.Popen(['qstat', '-f', str(jobID)],
                              stdout=subprocess.PIPE)
        out = ps.communicate()

        return "job_state = R" in str(out)

    def jobExists(self, jobID):

        ps = subprocess.Popen(['qstat', '-f', str(jobID)],
                              stdout=subprocess.PIPE)
        out = ps.communicate()

        return out[0] != ""

    def execute(self, working_directory):

        repeatSample = True
        while repeatSample:

            shutil.copytree(self.current_iteration_folder + "/_base_mutated",
                            working_directory)

            jobname = self.name + "-" + self.get_iteration_str() + "-" + \
                      self.get_sample_str()
            inplace_change(working_directory + "/Allrun", "#PBS -N .*",
                           "#PBS -N " + jobname + "\n")
            inplace_change(working_directory + "/Allrun", "#PBS -o .*",
                           "#PBS -o " + jobname + ".out" + "\n")

            cpus = self.getCurrentIterationParameterValue(P.CPU)

            hard_max_slots = 1
            if len(self.initial_config.nodeList) > 0:
                hard_max_slots = int(self.initial_config.ppn[
                                         self.initial_config.nodeList[0]] / 2)

            max_slots = int(cpus / (self.initial_config.nodes + 1))

            if max_slots > hard_max_slots: max_slots = hard_max_slots

            if len(self.initial_config.nodeList) > 0:
                mpirunH = "cat $PBS_NODEFILE | sort | uniq > knl+cpu_\n"
                mpirunH += "awk \'{print $0, \" max_slots=" + str(
                    max_slots) + "\"}\' knl+cpu_ > knl+cpu\n"
                mpirunH += "echo \"compute-002-1\" >> knl+cpu\n"
                # mpirunH = "echo \"`hostname` max_slots=" + str(max_slots) +
                #  "\" > knl+cpu; echo \"compute-002-1\" >> knl+cpu; "
            else:
                mpirunH = "echo \"compute-002-1\" > knl+cpu; "

            bwtest = mpirunH + "mpirun -np 4 --bynode " + self.netIface(
                "tcp") + " --hostfile knl+cpu /home/rr/opt/bandwith/bwTest > " \
                         "bwTest 2>&1\n\n"
            bwtest += "mpirun -np " + str(len(
                self.initial_config.nodeList)) + " --hostfile knl+cpu myPs\n\n"

            inplace_change(
                working_directory + "/Allrun",
                "<cd>",
                "cd " + working_directory + "\n\n" + bwtest)

            mpirun = mpirunH

            mpirun += "mpirun " + self.netIface("tcp")

            # mpirun += "--mca btl_base_verbose 30 --mca mtl_base_verbose 30 "
            mpirun += " -np " + str(cpus)
            if self.initial_config.bynode: mpirun += " --bynode"
            mpirun += " --hostfile knl+cpu " + self.initial_config.solver
            if (cpus > 1): mpirun += " -parallel"
            mpirun += " >> log." + self.initial_config.solver + " 2>&1"

            inplace_change(working_directory + "/Allrun",
                           "(runParallel|runParallel_tee|runApplication) " +
                           self.solver + " *[0-9]*",
                           mpirun)

            command = working_directory + "/Allclean"
            p = subprocess.Popen(command, shell=False, cwd=working_directory,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (stdoutdata, stderrdata) = p.communicate()

            self.myLog(stdoutdata)
            self.myLog(stderrdata)

            command = ["qsub", working_directory + "/Allrun"]

            self.myLog(open(working_directory + "/Allrun").read())
            p = subprocess.Popen(command, shell=False, cwd=working_directory,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)

            (stdoutdata, stderrdata) = p.communicate()

            self.myLog(' '.join(command))

            jobID = re.findall("(\d*)\.search6\.di\.uminho\.pt",
                               str(stdoutdata))
            self.myLog(jobID)

            if (jobID != []):

                jobID = jobID[0]

                log = working_directory + "/log." + self.solver
                attempts = 1000000  # check for log file attempts

                while attempts > 0 or self.jobRunning(jobID):

                    if os.path.isfile(working_directory + "/knl+cpu"):
                        killedDueToBlacklist = False
                        infile = open(working_directory + "/knl+cpu",
                                      'r').read()
                        print infile
                        if self.blacklist(infile):
                            # if "compute-641-8" in infile:
                            killedDueToBlacklist = True
                            break

                    if os.path.isfile(log):
                        start = time.time()
                        lastTime = 0  # last updated time
                        trigger = 0  # number of time check until kill,
                        # trigger * sleep is the time we wait basically
                        while self.jobRunning(jobID):
                            end = time.time()
                            elapsed = end - start
                            if elapsed > self.initial_config.walltimeH * 3600:
                                print "Terminating job by my walltime"
                                terminate = True
                                ps = subprocess.Popen(['qdel', jobID],
                                                      stdout=subprocess.PIPE)
                                ps.communicate()
                                break
                            sleep(60)
                            currentTime = os.path.getmtime(log)
                            self.myLog(
                                "last modified:" + time.ctime(currentTime))
                            if lastTime == currentTime:
                                trigger += 1
                                self.myLog(
                                    "Process seems stalled " + str(trigger))
                            else:
                                trigger = 0
                            if trigger > 15:
                                self.myLog("Process stalled!")
                                ps = subprocess.Popen(['qdel', jobID],
                                                      stdout=subprocess.PIPE)
                                ps.communicate()

                            lastTime = currentTime

                        break
                    attempts = attempts - 1
                    sleep(10)

            self.myLog(stdoutdata)
            self.myLog(stderrdata)

            ps = subprocess.Popen(['qdel', jobID], stdout=subprocess.PIPE)

            repeatSample = self.checkGracefullExecute()

            if killedDueToBlacklist:
                repeatSample = True
                print "\nkilled by blacklist..."
                sleep(60 * 4)

            if repeatSample:
                shutil.copytree(working_directory,
                                self.current_iteration_folder + "/failed/" +
                                self.get_sample_str() + str(
                                    int(round(time.time() * 1000))))
                self.deleteSampleFolder(working_directory)

            sleep(10)


class openfoam_loadManager_pbs_multi_node(openfoam_loadManager_pbs):

    def mutate(self):
        super(openfoam_loadManager_pbs_multi_node, self).mutate()

        pbs = "#!/bin/bash" + "\n" + "\n"
        pbs = pbs + "source load_build 2.4.0 dev Opt > /dev/null" + "\n"
        pbs = pbs + "<cd>" + "\n"

        inplace_change(
            self.current_iteration_folder + "/_base_mutated" + "/Allrun",
            "\#\!\/bin\/(?:sh|bash)", pbs)

    def jobRunning(self, jobID):
        running = True
        for j in jobID:
            if not self.jobExists(j):
                print "Checking if job is runnung but it does not exist"
                running = False
                break

            ps = subprocess.Popen(['qstat', '-f', str(j)],
                                  stdout=subprocess.PIPE)
            out = ps.communicate()

            running = "job_state = R" in str(out)
            if not running:
                break
        return running

    def jobExists(self, jobID):

        ps = subprocess.Popen(['qstat', '-f', str(jobID)],
                              stdout=subprocess.PIPE)
        out = ps.communicate()

        return out[0] != ""

    def post_finalize_iteration(self):

        super(openfoam_loadManager_pbs_multi_node,
              self).post_finalize_iteration()
        # shutil.move(working_directory + "/jobRunning", working_directory +
        # "/jobDone")

    def execute(self, working_directory):

        repeatSample = True
        while repeatSample:

            shutil.copytree(self.current_iteration_folder + "/_base_mutated",
                            working_directory)

            inplace_change(
                working_directory + "/Allrun",
                "<cd>",
                "cd " + working_directory)

            command = working_directory + "/Allclean"
            p = subprocess.Popen(command, shell=False, cwd=working_directory,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (stdoutdata, stderrdata) = p.communicate()

            self.myLog(stdoutdata)
            self.myLog(stderrdata)

            if os.path.exists(working_directory + "/jobRunning"):
                os.remove(working_directory + "/jobRunning")

            cpus = int(ceil(self.getCurrentIterationParameterValue(P.CPU) / (
                        len(self.initial_config.nodeList) * 1.0)))

            total_cpus = self.getCurrentIterationParameterValue(P.CPU)

            cpusPerNode = [0] * len(self.initial_config.nodeList)

            assigned = 0
            i = 0
            while assigned < total_cpus:
                hard_max_slots = int(self.initial_config.ppn[
                                         self.initial_config.nodeList[i]] / 2)
                if cpusPerNode[i] < hard_max_slots:
                    cpusPerNode[i] += 1
                    assigned += 1

                i += 1
                i = i % len(self.initial_config.nodeList)

            qsubs = open(working_directory + "/qsubs", 'w')
            self.jobIDs = []
            for i in range(len(self.initial_config.nodeList)):
                node = self.initial_config.nodeList[i]
                cpus = cpusPerNode[i]

                dummyJob = "#!/bin/bash\n\n"
                dummyJob = dummyJob + "#PBS -o " + working_directory + \
                           "/out.txt\n"
                dummyJob = dummyJob + "#PBS -e " + working_directory + \
                           "/err.txt\n\n"
                dummyJob = dummyJob + "echo \"`hostname` max_slots=" + str(
                    cpus) + "\" >> " + working_directory + "/jobRunning;" + "\n"
                dummyJob = dummyJob + "sleep 20" + "\n"
                dummyJob = dummyJob + "while true" + "\n"
                #                dummyJob = dummyJob + "while [ -f " +
                # working_directory + "/jobRunning ]"+ "\n"
                dummyJob = dummyJob + "do" + "\n"
                dummyJob = dummyJob + "    sleep 20" + "\n"
                dummyJob = dummyJob + "done" + "\n"
                dummyJob = dummyJob + "sudo cpupower frequency-set -g " \
                                      "ondemand" + "\n"

                dummyFile = '/dummy' + str(i) + '.pbs'
                f = open(working_directory + dummyFile, 'w')
                f.write(dummyJob)
                f.close()

                if "partial" in self.initial_config.mindHT:
                    request_ppn = cpus + int(self.initial_config.ppn[node] / 2)

                if "none" in self.initial_config.mindHT:
                    request_ppn = cpus

                if "full" in self.initial_config.mindHT:
                    request_ppn = int(self.initial_config.ppn[node])

                netRes = ""
                if (node == "r641" or node == "r431" or node == "r421") and (
                        self.initial_config.netInterface == "myri" or
                        self.initial_config.netInterface == "" or
                        self.initial_config.netInterface == "myri_simple"):
                    netRes = ":myri"

                command = ["qsub", "-l", "walltime=08:00:00,nodes=1:ppn=" +
                           str(request_ppn) + ":" + node + netRes,
                           working_directory + dummyFile]

                p = subprocess.Popen(command, shell=False,
                                     cwd=working_directory,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)

                (stdoutdata, stderrdata) = p.communicate()

                self.myLog(command)
                qsubs.write(' '.join(command) + "\n")
                jobID = re.findall("(\d*)\.search6\.di\.uminho\.pt",
                                   str(stdoutdata))
                if (jobID != []):
                    self.jobIDs.append(jobID[0])

                sleep(3)

            qsubs.close()

            self.myLog(self.jobIDs)
            jobsRunning = 0
            readyToRun = True
            if len(self.jobIDs) == len(self.initial_config.nodeList):
                for job in self.jobIDs:
                    attempts = 1000000
                    while attempts > 0 or self.jobRunning([job]):
                        if self.jobRunning([job]) and os.path.exists(
                                working_directory + "/jobRunning"):
                            jobsRunning = jobsRunning + 1;
                            break
                        if not self.jobExists(job):
                            print "Job does not exist"
                            break
                        attempts = attempts - 1
                        sleep(10)
                        if (attempts % 100) == 0:
                            print attempts
                            print self.jobIDs
                            print jobsRunning

            else:
                readyToRun = False

            sleep(2)

            if jobsRunning != len(self.initial_config.nodeList):
                readyToRun = False

            killedDueToBlacklist = False
            infile = open(working_directory + "/jobRunning", 'r').read()
            print infile
            if self.blacklist(infile):
                # if "compute-641-8" in infile:
                killedDueToBlacklist = True
                readyToRun = False

            file_ = open(working_directory + "/jobRunning", 'r')

            counts = collections.Counter(l.strip() for l in file_)
            if len(counts) != len(self.initial_config.nodeList):
                readyToRun = False
                print "nodefile node count does not match"
            else:
                for line, count in counts.most_common():
                    if count > 1: readyToRun = False

            if readyToRun:
                command = ["sort", "-r", working_directory + '/jobRunning',
                           "-o", working_directory + '/jobRunning']
                p = subprocess.Popen(command, shell=False,
                                     cwd=working_directory,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                (stdoutdata, stderrdata) = p.communicate()

                self.myLog(stdoutdata)
                self.myLog(stderrdata)

                self.myLog(open(working_directory + '/jobRunning', 'r').read())

                cpus = self.getCurrentIterationParameterValue(P.CPU)

                bwtest = "mpirun -np 4 --bynode " + self.netIface(
                    "myri") + " --hostfile " + working_directory + \
                         "/jobRunning /home/rr/opt/bandwith/bwTest > bwTest " \
                         "2>&1\n\n"
                bwtest += "mpirun -np " + str(len(
                    self.initial_config.nodeList)) + " --hostfile " + \
                          working_directory + "/jobRunning  myPs\n\n"

                mpirun = bwtest

                mpirun += "mpirun " + self.netIface("myri")
                # mpirun += "--mca btl_base_verbose 30 --mca mtl_base_verbose
                #  30 "
                mpirun += " -np " + str(cpus)
                if self.initial_config.bynode: mpirun += " --bynode"
                mpirun += " --hostfile " + working_directory + "/jobRunning " \
                          + self.initial_config.solver
                if (cpus > 1): mpirun += " -parallel "
                mpirun += " > log." + self.initial_config.solver + " 2>&1"

                inplace_change(working_directory + "/Allrun",
                               "(runParallel|runParallel_tee|runApplication) "
                               "" + self.solver + " *[0-9]*",
                               mpirun)

                inplace_change(working_directory + "/Allrun",
                               "(runParallel|runParallel_tee|runApplication) "
                               "*blockMesh",
                               "mpirun -np 1 --hostfile " + working_directory
                               + "/jobRunning blockMesh > log.blockMesh 2>&1")

                if self.initial_config.solver == "interDyMFoam":
                    inplace_change(working_directory + "/Allrun",
                                   "(runParallel|runParallel_tee|runApplication) *setFields",
                                   "mpirun -np 1 --hostfile " +
                                   working_directory + "/jobRunning setFields "
                                                       "> log.setFields 2>&1")

                if self.initial_config.solver == "simpleDyMFoam":
                    inplace_change(working_directory + "/Allrun",
                                   "(runParallel|runParallel_tee|runApplication) *surfaceFeatureExtract",
                                   "mpirun -np 1 --hostfile " +
                                   working_directory + "/jobRunning "
                                                       "surfaceFeatureExtract "
                                                       "> "
                                                       "log.surfaceFeatureExtract 2>&1")

                    inplace_change(working_directory + "/Allrun",
                                   "(runParallel|runParallel_tee|runApplication) *snappyHexMesh -overwrite",
                                   "mpirun -np 1 --hostfile " +
                                   working_directory + "/jobRunning "
                                                       "snappyHexMesh "
                                                       "-overwrite > "
                                                       "log.snappyHexMesh 2>&1")

                inplace_change(working_directory + "/Allrun",
                               "(runParallel|runParallel_tee|runApplication) "
                               "*checkMesh",
                               "mpirun -np 1 --hostfile " + working_directory
                               + "/jobRunning checkMesh > log.checkMesh 2>&1")

                inplace_change(working_directory + "/Allrun",
                               "(runParallel|runParallel_tee|runApplication) "
                               "*decomposePar",
                               "mpirun -np 1 --hostfile " + working_directory
                               + "/jobRunning decomposePar > log.decomposePar "
                                 "2>&1")

                command = ["Allrun"]
                p = subprocess.Popen(command, shell=False,
                                     cwd=working_directory,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)

                start = time.time()
                self.myLog(' '.join(command))

                log = working_directory + "/log." + self.solver
                attempts = 1000
                terminate = False
                while attempts > 0 and p.poll() == None and self.jobRunning(
                        self.jobIDs) and not terminate:
                    print "Waiting on prep..."
                    if os.path.isfile(log):
                        print "Solver running..."
                        # fh = open(log)
                        lastTime = 0  # last updated time
                        trigger = 0  # number of time check until kill,
                        # trigger * sleep is the time we wait basically
                        while p.poll() == None and self.jobRunning(self.jobIDs):
                            end = time.time()
                            elapsed = end - start
                            if elapsed > self.initial_config.walltimeH * 3600:
                                print "Terminating job by my walltime"
                                terminate = True
                                break
                            sleep(60)
                            # subprocess.call(["tail", "-n 2", log])
                            currentTime = os.path.getmtime(log)
                            self.myLog(
                                "last modified:" + time.ctime(currentTime))
                            if lastTime == currentTime:
                                trigger += 1
                                self.myLog(
                                    "Process seems stalled " + str(trigger))
                            else:
                                trigger = 0
                            if trigger > 15:
                                self.myLog("Process stalled!")
                                ps = subprocess.Popen(['qdel', self.jobIDs[0]],
                                                      stdout=subprocess.PIPE)
                                ps.communicate()

                            lastTime = currentTime

                        break
                    attempts = attempts - 1
                    sleep(10)

                if not self.jobRunning(self.jobIDs) or terminate: p.kill()
                print "job terminated"
                (stdoutdata, stderrdata) = p.communicate()

            else:
                self.myLog("\njob configuration or submission failed\n")

            self.myLog(stdoutdata)
            self.myLog(stderrdata)

            ############ post-execute #########

            repeatSample = self.checkGracefullExecute()

            for job in self.jobIDs:
                ps = subprocess.Popen(['qdel', job], stdout=subprocess.PIPE)

            if killedDueToBlacklist:
                repeatSample = True
                print "\nkilled by blacklist..."
                sleep(60 * 4)

            if repeatSample:
                shutil.copytree(working_directory,
                                self.current_iteration_folder + "/failed/" +
                                self.get_sample_str() + str(
                                    int(round(time.time() * 1000))))
                self.deleteSampleFolder(working_directory)

            sleep(10)


class openfoam_loadManager_pbs_single_job(openfoam_loadManager_pbs):

    def mutate(self):
        super(openfoam_loadManager_pbs_single_job, self).mutate()

        pbs = "#!/bin/bash" + "\n" + "\n"
        pbs = pbs + "source load_build 2.4.0 dev Opt > /dev/null" + "\n"
        pbs = pbs + "<cd>" + "\n"

        inplace_change(
            self.current_iteration_folder + "/_base_mutated" + "/Allrun",
            "\#\!\/bin\/(?:sh|bash)", pbs)

    def jobRunning(self, jobID):
        running = True
        for j in jobID:
            if not self.jobExists(j):
                print "Checking if job is runnung but it does not exist"
                running = False
                break

            ps = subprocess.Popen(['qstat', '-f', str(j)],
                                  stdout=subprocess.PIPE)
            out = ps.communicate()

            running = "job_state = R" in str(out)
            if not running:
                break
        return running

    def jobExists(self, jobID):

        ps = subprocess.Popen(['qstat', '-f', str(jobID)],
                              stdout=subprocess.PIPE)
        out = ps.communicate()

        return out[0] != ""

    def post_finalize_iteration(self):

        super(openfoam_loadManager_pbs_single_job,
              self).post_finalize_iteration()
        # shutil.move(working_directory + "/jobRunning", working_directory +
        # "/jobDone")

    def execute(self, working_directory):

        repeatSample = True
        while repeatSample:

            shutil.copytree(self.current_iteration_folder + "/_base_mutated",
                            working_directory)

            inplace_change(
                working_directory + "/Allrun",
                "<cd>",
                "cd " + working_directory)

            command = working_directory + "/Allclean"
            p = subprocess.Popen(command, shell=False, cwd=working_directory,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (stdoutdata, stderrdata) = p.communicate()

            self.myLog(stdoutdata)
            self.myLog(stderrdata)

            if os.path.exists(working_directory + "/jobRunning"):
                os.remove(working_directory + "/jobRunning")

            nodes = len(self.initial_config.nodeList)

            cpus = int(ceil(
                self.getCurrentIterationParameterValue(P.CPU) / (nodes * 1.0)))

            total_cpus = self.getCurrentIterationParameterValue(P.CPU)

            cpusPerNode = [0] * nodes

            assigned = 0
            i = 0
            while assigned < total_cpus:
                hard_max_slots = int(self.initial_config.ppn[
                                         self.initial_config.nodeList[i]] / 2)
                if cpusPerNode[i] < hard_max_slots:
                    cpusPerNode[i] += 1
                    assigned += 1

                i += 1
                i = i % nodes

            qsubs = open(working_directory + "/qsubs", 'w')
            self.jobIDs = []
            for i in range(1):
                node = self.initial_config.nodeList[i]
                cpus = cpusPerNode[i]

                dummyJob = "#!/bin/bash\n\n"
                dummyJob = dummyJob + "#PBS -o " + working_directory + \
                           "/out.txt\n"
                dummyJob = dummyJob + "#PBS -e " + working_directory + \
                           "/err.txt\n\n"

                # dummyJob = dummyJob + "echo \"`hostname` max_slots=" + str(
                # cpus) + "\" >> " + working_directory + "/jobRunning;" + "\n"

                dummyJob = dummyJob + "cat $PBS_NODEFILE | sort | uniq > " + \
                           working_directory + "/jobRunning;" + "\n"

                dummyJob = dummyJob + "sleep 20" + "\n"
                dummyJob = dummyJob + "while true" + "\n"
                #                dummyJob = dummyJob + "while [ -f " +
                # working_directory + "/jobRunning ]"+ "\n"
                dummyJob = dummyJob + "do" + "\n"
                dummyJob = dummyJob + "    sleep 20" + "\n"
                dummyJob = dummyJob + "done" + "\n"
                dummyJob = dummyJob + "sudo cpupower frequency-set -g " \
                                      "ondemand" + "\n"

                dummyFile = '/dummy' + str(i) + '.pbs'
                f = open(working_directory + dummyFile, 'w')
                f.write(dummyJob)
                f.close()

                if "partial" in self.initial_config.mindHT:
                    request_ppn = cpus + int(self.initial_config.ppn[node] / 2)

                if "none" in self.initial_config.mindHT:
                    request_ppn = cpus

                if "full" in self.initial_config.mindHT:
                    request_ppn = int(self.initial_config.ppn[node])

                netRes = ""
                if (node == "r641" or node == "r431" or node == "r421") and (
                        self.initial_config.netInterface == "myri" or
                        self.initial_config.netInterface == "" or
                        self.initial_config.netInterface == "myri_simple"):
                    netRes = ":myri"

                command = ["qsub", "-l",
                           "walltime=02:30:00,nodes=" + str(nodes) + ":ppn=" +
                           str(request_ppn) + ":" + node + netRes,
                           working_directory + dummyFile]

                p = subprocess.Popen(command, shell=False,
                                     cwd=working_directory,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)

                (stdoutdata, stderrdata) = p.communicate()

                self.myLog(command)
                qsubs.write(' '.join(command) + "\n")
                jobID = re.findall("(\d*)\.search6\.di\.uminho\.pt",
                                   str(stdoutdata))
                if (jobID != []):
                    self.jobIDs.append(jobID[0])

                sleep(3)

            qsubs.close()

            self.myLog(self.jobIDs)
            jobsRunning = 0
            readyToRun = True
            if len(self.jobIDs) == 1:
                for job in self.jobIDs:
                    attempts = 1000000
                    while attempts > 0 or self.jobRunning([job]):
                        if self.jobRunning([job]) and os.path.exists(
                                working_directory + "/jobRunning"):
                            jobsRunning = jobsRunning + 1;
                            break
                        if not self.jobExists(job):
                            print "Job does not exist"
                            break
                        attempts = attempts - 1
                        sleep(30)
                        if (attempts % 100) == 0:
                            print attempts
                            print self.jobIDs
                            print jobsRunning

            else:
                readyToRun = False

            sleep(2)

            if jobsRunning != 1:
                readyToRun = False

            killedDueToBlacklist = False
            infile = open(working_directory + "/jobRunning", 'r').read()
            print infile
            if self.blacklist(infile):
                # if "compute-641-8" in infile:
                killedDueToBlacklist = True
                readyToRun = False

            # file_ = open(working_directory + "/jobRunning", 'r')

            # counts = collections.Counter(l.strip() for l in file_)
            # if len(counts) != len(self.initial_config.nodeList):
            #     readyToRun = False
            #     print "nodefile node count does not match"
            # else:
            #     for line, count in counts.most_common():
            #         if count > 1: readyToRun = False

            if readyToRun:

                with open(working_directory + "/jobRunning", 'r') as f:
                    file_lines = [''.join(
                        [x.strip(), " max_slots=" + str(cpusPerNode[0]), '\n'])
                                  for x in f.readlines()]

                with open(working_directory + "/jobRunning", 'w') as f:
                    f.writelines(file_lines)

                command = ["sort", "-r", working_directory + '/jobRunning',
                           "-o", working_directory + '/jobRunning']
                p = subprocess.Popen(command, shell=False,
                                     cwd=working_directory,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
                (stdoutdata, stderrdata) = p.communicate()

                self.myLog(stdoutdata)
                self.myLog(stderrdata)

                self.myLog(open(working_directory + '/jobRunning', 'r').read())

                cpus = self.getCurrentIterationParameterValue(P.CPU)

                bwtest = "mpirun -np " + str(
                    nodes) + " --bynode " + self.netIface(
                    "myri") + " --hostfile " + working_directory + \
                         "/jobRunning /home/rr/opt/bandwith/bwTest > bwTest " \
                         "2>&1\n\n"

                bwtest += "mpirun -np " + str(
                    nodes) + " --bynode --hostfile " + working_directory + \
                          "/jobRunning myPs\n\n"

                mpirun = bwtest

                mpirun += "mpirun " + self.netIface("myri")
                # mpirun += "--mca btl_base_verbose 30 --mca mtl_base_verbose
                #  30 "
                mpirun += " -np " + str(cpus)
                if self.initial_config.bynode: mpirun += " --bynode"
                mpirun += " --hostfile " + working_directory + "/jobRunning " \
                          + self.initial_config.solver
                if (cpus > 1): mpirun += " -parallel "
                mpirun += " > log." + self.initial_config.solver + " 2>&1"

                inplace_change(working_directory + "/Allrun",
                               "(runParallel|runParallel_tee|runApplication) "
                               "" + self.solver + " *[0-9]*",
                               mpirun)

                inplace_change(working_directory + "/Allrun",
                               "(runParallel|runParallel_tee|runApplication) "
                               "*blockMesh",
                               "mpirun -np 1 --hostfile " + working_directory
                               + "/jobRunning blockMesh > log.blockMesh 2>&1")

                if self.initial_config.solver == "interDyMFoam":
                    inplace_change(working_directory + "/Allrun",
                                   "(runParallel|runParallel_tee|runApplication) *setFields",
                                   "mpirun -np 1 --hostfile " +
                                   working_directory + "/jobRunning setFields "
                                                       "> log.setFields 2>&1")

                if self.initial_config.solver == "simpleDyMFoam":
                    inplace_change(working_directory + "/Allrun",
                                   "(runParallel|runParallel_tee|runApplication) *surfaceFeatureExtract",
                                   "mpirun -np 1 --hostfile " +
                                   working_directory + "/jobRunning "
                                                       "surfaceFeatureExtract "
                                                       "> "
                                                       "log.surfaceFeatureExtract 2>&1")

                    inplace_change(working_directory + "/Allrun",
                                   "(runParallel|runParallel_tee|runApplication) *snappyHexMesh -overwrite",
                                   "mpirun -np 1 --hostfile " +
                                   working_directory + "/jobRunning "
                                                       "snappyHexMesh "
                                                       "-overwrite > "
                                                       "log.snappyHexMesh 2>&1")

                inplace_change(working_directory + "/Allrun",
                               "(runParallel|runParallel_tee|runApplication) "
                               "*checkMesh",
                               "mpirun -np 1 --hostfile " + working_directory
                               + "/jobRunning checkMesh > log.checkMesh 2>&1")

                inplace_change(working_directory + "/Allrun",
                               "(runParallel|runParallel_tee|runApplication) "
                               "*decomposePar",
                               "mpirun -np 1 --hostfile " + working_directory
                               + "/jobRunning decomposePar > log.decomposePar "
                                 "2>&1")

                command = ["Allrun"]
                p = subprocess.Popen(command, shell=False,
                                     cwd=working_directory,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)

                start = time.time()
                self.myLog(' '.join(command))

                log = working_directory + "/log." + self.solver
                attempts = 1000
                terminate = False
                while attempts > 0 and p.poll() == None and self.jobRunning(
                        self.jobIDs) and not terminate:
                    print "Waiting on prep..."
                    if os.path.isfile(log):
                        print "Solver running..."
                        # fh = open(log)
                        lastTime = 0  # last updated time
                        trigger = 0  # number of time check until kill,
                        # trigger * sleep is the time we wait basically
                        while p.poll() == None and self.jobRunning(self.jobIDs):
                            end = time.time()
                            elapsed = end - start
                            if elapsed > self.initial_config.walltimeH * 3600:
                                print "Terminating job by my walltime"
                                terminate = True
                                break
                            sleep(60)
                            # subprocess.call(["tail", "-n 2", log])
                            currentTime = os.path.getmtime(log)
                            self.myLog(
                                "last modified:" + time.ctime(currentTime))
                            if lastTime == currentTime:
                                trigger += 1
                                self.myLog(
                                    "Process seems stalled " + str(trigger))
                            else:
                                trigger = 0
                            if trigger > 15:
                                self.myLog("Process stalled!")
                                ps = subprocess.Popen(['qdel', self.jobIDs[0]],
                                                      stdout=subprocess.PIPE)
                                ps.communicate()

                            lastTime = currentTime

                        break
                    attempts = attempts - 1
                    sleep(10)

                if not self.jobRunning(self.jobIDs) or terminate: p.kill()
                print "job terminated"
                (stdoutdata, stderrdata) = p.communicate()

            else:
                self.myLog("\njob configuration or submission failed\n")

            self.myLog(stdoutdata)
            self.myLog(stderrdata)

            ############ post-execute #########

            repeatSample = self.checkGracefullExecute()

            for job in self.jobIDs:
                ps = subprocess.Popen(['qdel', job], stdout=subprocess.PIPE)

            if killedDueToBlacklist:
                repeatSample = True
                print "\nkilled by blacklist..."
                sleep(60 * 4)

            if repeatSample:
                shutil.copytree(working_directory,
                                self.current_iteration_folder + "/failed/" +
                                self.get_sample_str() + str(
                                    int(round(time.time() * 1000))))
                self.deleteSampleFolder(working_directory)

            sleep(10)


class OpenFOAM_Config:

    def __init__(self, session_path, entry_name, solver, foam_case_path):
        self.session_path = session_path
        self.entry_name = entry_name
        self.solver = solver
        self.cellCountMethod = "analytical-damBreak"
        self.foam_case_path = foam_case_path
        self.samples = 1
        self.failCount = 3
        self.pushbullet = False
        self.netInterface = ""
        self.mindHT = "full"
        self.bynode = False
        self.walltimeH = 4
        self.ppn = dict()
        self.ppn["r781"] = 64
        self.ppn["r771"] = 56
        self.ppn["r662"] = 48
        self.ppn["r652"] = 40
        self.ppn["r641"] = 32
        self.ppn["r541"] = 32
        self.ppn["r432"] = 24
        self.ppn["r431"] = 24
        self.ppn["r421"] = 16
        self.ppn["r321"] = 8
        self.table = OrderedDict()
        self.table[P.HETEROGENEITY] = [0]
        self.blockMeshDictLocation = "/constant/polyMesh/"
        if "OF-dev" in foam_case_path: self.blockMeshDictLocation = "/system/"



import subprocess


nodes = ["compute-421-1",
        "compute-421-2",
        "compute-421-3",
        "compute-421-4",
        "compute-641-10",
        "compute-641-11",
        "compute-641-12",
#        "compute-641-13",
        "compute-641-15",
        "compute-641-16",
        "compute-641-18",
        "compute-641-19",
        "compute-641-1",
        "compute-641-20",
        "compute-641-2",
        "compute-641-3",
        "compute-641-4",
        "compute-641-5",
        "compute-641-6",
        "compute-641-7",
        "compute-641-8",
        "compute-641-9",
        "compute-002-1"]



#nodes = ["localhost"]

for node in nodes:

    command = ["mpirun", "-np", "1", "--host", node, "sudo", "cpupower",
               "frequency-info", "-p"]

    print " ".join(command)

    p = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)

    (stdoutdata, stderrdata) = p.communicate()

    print stdoutdata
    print stderrdata

    if stderrdata == "":

    
        command = ["mpirun", "-np", "1", "--host", node , "sudo", "cpupower", "frequency-set", "-g", "ondemand"]

        print " ".join(command)

        p = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE , stderr=subprocess.PIPE)

        (stdoutdata, stderrdata) = p.communicate()

        #print stdoutdata
        #print stderrdata

        command = ["mpirun", "-np", "1", "--host", node, "sudo", "cpupower", "-c",
                   "0", "frequency-info", "-l"]

        print " ".join(command)

        p = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        (stdoutdata, stderrdata) = p.communicate()

        #print stdoutdata
        #print stderrdata

        values = stdoutdata.split("\n")[1].split(" ")

        print values

        command = ["mpirun", "-np", "1", "--host", node, "sudo", "cpupower",
                   "-c", "all", "frequency-set", "--min", values[0]]

        print " ".join(command)

        p = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        (stdoutdata, stderrdata) = p.communicate()

        #print stdoutdata
        #print stderrdata

        command = ["mpirun", "-np", "1", "--host", node, "sudo", "cpupower",
                   "-c", "all", "frequency-set", "--max", values[1]]

        print " ".join(command)

        p = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        (stdoutdata, stderrdata) = p.communicate()

        command = ["mpirun", "-np", "1", "--host", node, "sudo", "cpupower",
                   "frequency-info", "-p"]

        print " ".join(command)

        p = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        (stdoutdata, stderrdata) = p.communicate()

        print stdoutdata
        print stderrdata

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import re
import sys
import os
import numpy as np
import pprint
from mpl_toolkits.mplot3d import Axes3D
import ast
from collections import OrderedDict
from openfoam import P
from cycler import cycler
import traceback
from sets import Set

def parseToPlot( log):
        """Return an array of dicts. Each dict entry is a property in the log 
        that has a range x proportional to the number of entries and a matrix
         with the results"""
        data = open(log).read()
        data=re.split('\n\n', data)
        
        dataD= dict()
        for entry in data:
            if str(entry) != '':
                if "Balance-episode" in entry:
                    entry = entry.replace("Balance-episode-", "Balance_episode,").replace("-in-iteration-",",")
                    entry = entry.replace("Distributing...", "1").replace("No_balance_needed","0")
                            
                spp = entry.split()
                key = spp[0].split(",")[0]
                dataD[key] = spp
        
        for key, m in dataD.iteritems():
            
            for r in range(len(m)):
                m[r] = m[r].split(",")
                m[r].pop(0)

                for i in range(len(m[r])):
                    try:
                        m[r][i]=float(m[r][i])
                    except Exception:
                        continue
            if m != [['']]: 
                dataD[key]= (range(1,len(m)+1), [list(i) for i in zip(*m)])
            #if len(dataD[key][1])==1:
            #    dataD[key]=(dataD[key][0],dataD[key][1][0])
            
            
        
        #mean across ranks                             
        dataD["Busy(%)"] = (dataD["Busy(%)"][0],[np.mean(np.array(dataD["Busy(%)"][1]),axis=0).tolist()])
        dataD["Idle(%)"] = (dataD["Idle(%)"][0],[np.mean(np.array(dataD["Idle(%)"][1]),axis=0).tolist()])
        print "TODO check values Number_of_cells_RSD(%)" #, ddof=1
        dataD["Number_of_cells_RSD(%)"] = (dataD["Number_of_cells"][0],[(100*np.std(np.array(dataD["Number_of_cells"][1]),axis=0)/np.mean(np.array(dataD["Number_of_cells"][1]),axis=0)).tolist()])
        dataD.pop("Number_of_cells")
        
        
        return dataD
    
    
def to_iteration_space(initial_period, values):
    y=[]
    for x in values:
        y.append((x-1)*int(initial_period/2))
        
    y [0] = 2
    return y

def on_vs_off_xaxis_time(logs, entryResultsFilePath):    

        data=[]
        for log in logs:
            data.append(parseToPlot(log[1]))
           
        
        headers = open(entryResultsFilePath.replace("-results", "-config"), 'r').read()
        headers = re.split('\n', headers)
        headersData = dict()
        for header in headers:
            if header != "":
                headersData[header.split("=")[0]] = ast.literal_eval(header.split("=")[1])
        
        balance_epidoes=[]
        
        for i in range(len(data)):
            if not "Balance_episode" in data[i].keys():
                continue
            eps = data[i]["Balance_episode"][1]
            for j in range(len(eps[2])):
                if eps[2][j] == 1.0:
                    balance_epidoes.append(eps[1][j])
        
        #markers = [(2+i/2, 1+i%2, 0) for i in range(16)]
        markers = ['x', 'o', '^', 's', '8', '+', '.']
        colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
              '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
              '#bcbd22', '#17becf']

        plotID=1
        plotRows = 5
        plotCols = 2
        
        fig = plt.figure( figsize=(9,12))  # create figure & 1 axis
        

        all_keys = Set()
               
        for x in data:
            for y in x.iteritems():
                all_keys.add(y[0])
        
        for key in all_keys:
                            
                if key == "sample" or key == "Balance_episode": continue
                if "weights" in key : continue
                
                print key
                try:
                    current=[]
                    x=[]
                    lines=[]

                    #get all runs key property together

                    for i in range(len(data)):
                        if not key in data[i].keys():
                            continue
                        current.append(data[i][key])
                        x.append(data[i][key][0])
                        lines.append(data[i][key][1][0])
                        
                except Exception:
                    print "no key in all data " + key
                    continue
                                      
                
                try:
                    
                    if key == "Idle_time" or key == "Busy_time" or key == "loadBalanceCells" or key == "updateLoadManager":
                        continue
#                         ax = fig.add_subplot(plotRows,plotCols,plotID, projection='3d')
# 
#                         X = np.arange(0, 8, 1)
#                         Y = np.arange(0, len(current[2][0]), 1)
#                         X, Y = np.meshgrid(X, Y)
#                        
#                         Z =np.transpose(current[2][1])
#                         
#                         surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
#                                                linewidth=0.1, antialiased=False)
                                        
                    if key == "full" or key == "SolvingProblem" or key == "SolvingSim":
                        linesAdded=[]
                        ax = fig.add_subplot(plotRows,plotCols,plotID)
                        
                        for xc in balance_epidoes:
                            ax.axvline(x=xc, lw=0.2, color='k')
                            
                        markersIt = iter(markers)
                        colorIt = iter(colors)
                        ax.set_title(key, size=10)
                        
                        ax.set_xlabel('iteration')
                        ax.set_ylabel('seconds')
                                            
                        for i in range(len(lines)):
                            linesAdded = linesAdded + ax.plot(to_iteration_space(headersData["balance-period"][0], x[i]), lines[i], label=logs[i][0],
                                    marker=next(markersIt), 
                                    color=next(colorIt),
                                    markersize=4,
                                    markevery=10,
                                    linewidth=0.8)
                        
                        if len(logs) == 2:
                            i = 1 if P.LOAD_MANAGER.value  + ".True" in logs[0][0] else 0
                            j = 0 if P.LOAD_MANAGER.value  + ".True" in logs[0][0] else 1            
                            gain= [ xx/yy for xx, yy in zip(lines[i],lines[j])]
                            
                            ax2 = ax.twinx()
                            linesAdded = linesAdded + ax2.plot(to_iteration_space(headersData["balance-period"][0],x[0]), gain, label="gain",
                                        marker=next(markersIt), 
                                        color=next(colorIt),
                                        markersize=4,
                                        markevery=10,
                                        linewidth=0.8)
                            
                            ax2.set_ylim([0.5,3.0])
                            ax2.grid(False)
                            ax2.set_ylabel('gain')

                        
                    if key == "balance-period-update":
                        linesAdded=[]
                        ax = fig.add_subplot(plotRows,plotCols,plotID)
                        for xc in balance_epidoes:
                            ax.axvline(x=xc, lw=0.2, color='k')
                        markersIt = iter(markers)
                        colorIt = iter(colors)
                        ax.set_title(key, size=10)
                        ax.set_xlabel('iteration')
                        ax.set_ylabel('balance period')                    
                        for i in range(len(lines)):
                            linesAdded = linesAdded + ax.plot(x[i], lines[i], label=logs[i][0],
                                    marker=next(markersIt), 
                                    color=next(colorIt),
                                    markersize=4,
                                    markevery=10,
                                    linewidth=0.8)

                    else:    
                        linesAdded=[]
                        ax = fig.add_subplot(plotRows,plotCols,plotID)
                        for xc in balance_epidoes:
                            ax.axvline(x=xc, lw=0.2, color='k')
                        markersIt = iter(markers)
                        colorIt = iter(colors)
                        ax.set_title(key, size=10)
                        ax.set_xlabel('iteration')                    
                        for i in range(len(lines)):
                            linesAdded = linesAdded + ax.plot(to_iteration_space(headersData["balance-period"][0],x[i]), lines[i], label=logs[i][0],
                                    marker=next(markersIt), 
                                    color=next(colorIt),
                                    markersize=4,
                                    markevery=10,
                                    linewidth=0.8)
                            
                        if "%" in key:
                            ax.set_ylim([0,200])
                            ax.set_ylabel('%')
                            
                        if key == "balance-period-update":
                            ax.set_ylabel('balance period')


                except Exception as e:
                    print "poltting " + key + ": " + str(e)
                    print(traceback.format_exc())

                    
                #ax.legend(loc=9,ncol=2, mode="expand", borderaxespad=0., bbox_to_anchor=(0., 1.02, 1., .102))
                
                
                
                ax.legend(linesAdded, [l.get_label() for l in linesAdded],ncol=1)
                plotID = plotID+1

        
        return fig, plt
        
def weak_scal(result_file):
    
        
    
            
    data = open(result_file).read()
    data = re.split('\n\n', data)
    
    dataD = dict()
    for entry in data:
        if str(entry) != '':
            lines = entry.split("\n")
            key = lines[0]
            lines.pop(0)
            m = [ line.replace(" ", "").split("|") for line in lines]
            m_r = np.delete(m, (0, 1), axis=0)
            m = np.delete(m_r, (0, 1, 2), axis=1)
            
            dataD[key] = m[:, :-1].astype(float).tolist()
    
    
    
    headers = open(result_file.replace("-results", "-config"), 'r').read()
    headers = re.split('\n', headers)
    headersData = dict()
    for header in headers:
        if header != "":
            headersData[header.split("=")[0]] = ast.literal_eval(header.split("=")[1])
        
    
    
    
    markers = ['x', 'o', '^', 's', '8', '+', '.']
    colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
              '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
              '#bcbd22', '#17becf']

    fig = plt.figure(figsize=(9, 12))  # create figure & 1 axis
    plotRows = 4
    plotCols = 2
            
    ax = fig.add_subplot(plotRows, plotCols, 1)
    ax.set_title("nSharma", size=10)  
    
    
    plt.xticks(range(len(headersData["CPUs"])), headersData["CPUs"])
    
    markersIt = iter(markers)     
    colorIt = iter(colors)

    linesAdded = ax.plot(range(len(headersData["CPUs"])), dataD["SolvingProblem"][0], label="on", 
                                    marker=next(markersIt), 
                                    color=next(colorIt),
                                    markersize=4,
                                    linewidth=0.8)                
        
    linesAdded= linesAdded + ax.plot(range(len(headersData["CPUs"])), dataD["SolvingProblem"][1], label="off", 
                                    marker=next(markersIt), 
                                    color=next(colorIt),
                                    markersize=4,
                                    linewidth=0.8) 
    
    
    ax.set_ylabel('seconds')
    ax.set_xlabel('(#proc, #cell_count*1024)')
  
    ax2 = ax.twinx()
    ax2.grid(False)
    
    gain = [ xx / yy for xx, yy in zip(dataD["SolvingProblem"][1], dataD["SolvingProblem"][0])]
    linesAdded= linesAdded +  ax2.plot(range(len(headersData["CPUs"])), gain, label="gain",
                                    marker=next(markersIt), 
                                    color=next(colorIt),
                                    markersize=4,
                                    linewidth=0.8,
                                    ls=':')
    
    
    ax2.set_ylabel('gain')
    ax2.set_ylim([0.0,2.0])
    ax.legend(linesAdded, [l.get_label() for l in linesAdded], ncol=1)

    return fig, plt
        
def main_weak_scal(root):
         
     
    #with PdfPages(root[0] + "/../weak_scal.pdf") as pdf:
    fig, plt = weak_scal(root[0] + "/" + root[1])      
    fig.tight_layout()
    pdf.savefig()
    plt.close(fig)
    
    
    
         
         
##############################
         
         
font = { 'size'   : 7}
plt.rc('font', **font)
plt.rcParams['axes.grid']= False
plt.rc('grid',  ls=':')
#plt.rc('lines', lw=2, color='g')
plt.rcParams['legend.loc']= 'best'
plt.rcParams['legend.fontsize']= 'x-small'
plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y'])))


#root=sys.argv[1]


root = "/home/rr/work/openfoam/run/benchmarktool/benchmarks/"
root += "search6-knl+cpu.1495632073208"

entries = [ name for name in os.listdir(root) if os.path.isdir(os.path.join(root, name)) and "base" not in name ]

for entry in entries:
    entryPath = root + "/" + entry 
    runs = [ name for name in os.listdir(entryPath) if os.path.isdir(os.path.join(entryPath, name)) ]
    
    fileres = entryPath + "/" + entry + "-config.txt"
    headers = open(fileres, 'r').read()
    headers = re.split('\n', headers)
    headersData = dict()
    for header in headers:
        if header != "":
            headersData[header.split("=")[0]] = ast.literal_eval(header.split("=")[1])
    
    
    
    for cpuR in headersData["CPUs"]:
        param = str(cpuR[0]) + "," + str(cpuR[1]) + "-";
        f = "nSharma.False_CPUs.-" + param
        t = "nSharma.True_CPUs.-" + param
        tpath = entryPath + "/" + t + "/"+ t + "-results.txt"
        fpath = entryPath + "/" + f + "/"+ f + "-results.txt"
        
        if os.path.isfile(tpath) and os.path.isfile(fpath):
            with PdfPages(root + "/" + entry + "-" + param + ".pdf") as pdf:   
                fig, plt = on_vs_off_xaxis_time([(t,tpath),(f,fpath)], entryPath + "/" + entry + "-results.txt")
                fig.tight_layout()
                pdf.savefig()
                plt.close(fig)                
                fig = plt.figure(figsize=(9,12))
                fig.text(.1,.0,root + "\n\n" + open(root + "/main.py").read(),fontsize=6)
                pdf.savefig()
                plt.close(fig)
    
    
    
    
    
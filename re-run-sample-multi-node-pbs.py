import sys

import numpy
import itertools
import shutil
import subprocess
import time
import os
import re
import signal
import traceback
import collections

from enum import Enum, unique
from collections import OrderedDict
from tabulate import tabulate
from time import sleep  

def jobRunning(jobID):
    running = True
    for j in jobID:
        if not jobExists(j):
                print "Checking if job is runnung but it does not exist"
                running = False
                break

        ps = subprocess.Popen(['qstat', '-f', str(j)], stdout=subprocess.PIPE)
        out = ps.communicate()
                
        running = "job_state = R" in str(out)
        if not running:
                break
    return running

def jobExists(jobID):

    ps = subprocess.Popen(['qstat', '-f', str(jobID)], stdout=subprocess.PIPE)
    out = ps.communicate()

    return out[0] != ""
   
working_directory = os.getcwd()
solver = "interDyMFoam"
qsubs = open( working_directory + "/qsubs", 'r').read()
qsubs=qsubs.split("\n")
del qsubs[-1]
print qsubs

command = working_directory + "/Allclean" 
p = subprocess.Popen(command, shell=False, cwd=working_directory,
                       stdout=subprocess.PIPE , stderr=subprocess.PIPE)
(stdoutdata, stderrdata) = p.communicate()

print stdoutdata
print stderrdata

if os.path.exists(working_directory + "/jobRunning"):
        os.remove(working_directory + "/jobRunning")
                         

jobIDs = []
for i in range(len(qsubs)):
    
    p = subprocess.Popen(qsubs[i].split(" "), shell=False, cwd=working_directory,
                           stdout=subprocess.PIPE , stderr=subprocess.PIPE)
    
    (stdoutdata, stderrdata) = p.communicate()

    print qsubs[i]
    jobID = re.findall("(\d*)\.search6\.di\.uminho\.pt", str(stdoutdata))
    if (jobID != []):
        jobIDs.append(jobID[0])

    sleep(3)
        
                   
print jobIDs
jobsRunning = 0
readyToRun = True
if len(jobIDs) == len(qsubs):
    for job in jobIDs:
        attempts = 1000000
        while attempts > 0 or jobRunning([job]):
            if jobRunning([job]) and os.path.exists(working_directory + "/jobRunning"):
                jobsRunning = jobsRunning + 1;
                break        
            if not jobExists(job):
                print "Job does not exist"
                break  
            attempts = attempts - 1
            sleep(10) 
            if (attempts % 100) == 0: 
                print attempts
                print jobIDs
                print jobsRunning

else:
    readyToRun = False
    
sleep(2)

if jobsRunning != len(qsubs):
    readyToRun = False


infile = open(working_directory + "/jobRunning", 'r').read()
print infile
    
file_ = open(working_directory + "/jobRunning", 'r')   
                           
counts = collections.Counter(l.strip() for l in file_)
if len(counts) != len(qsubs):
    readyToRun = False
    print "nodefile node count does not match"
else:
    for line, count in counts.most_common():
        if count > 1 : readyToRun = False

if readyToRun:
    command = ["sort", "-r", working_directory + '/jobRunning', "-o", working_directory + '/jobRunning']
    p = subprocess.Popen(command, shell=False, cwd=working_directory,
                       stdout=subprocess.PIPE , stderr=subprocess.PIPE)
    (stdoutdata, stderrdata) = p.communicate()

    print(stdoutdata)
    print(stderrdata)

    print(open(working_directory + '/jobRunning', 'r').read())
   

    command = ["Allrun"]            
    p = subprocess.Popen(command, shell=False, cwd=working_directory,
                           stdout=subprocess.PIPE , stderr=subprocess.PIPE)
     
                 
    print(' '.join(command))
    
    log = working_directory + "/log." + solver
    attempts = 1000
    start = time.time()
    terminate= False
    while attempts > 0 and p.poll() == None and jobRunning(jobIDs) and not terminate:
        print "Waiting on prep..."
        if os.path.isfile(log):
            print "Solver running..."
            # fh = open(log)
            lastTime = 0  # last updated time
            trigger = 0  # number of time check until kill, trigger * sleep is the time we wait basically
            while p.poll() == None and jobRunning(jobIDs):
                end = time.time()
                elapsed = end-start
                if elapsed > 2*3600:
                    print "Terminating job by my walltime"
                    terminate = True
                    break
                sleep(60)
                # subprocess.call(["tail", "-n 2", log])
                currentTime = os.path.getmtime(log)
                print("last modified:" + time.ctime(currentTime))                    
                if lastTime == currentTime:
                    trigger += 1
                    print("Process seems stalled " + str(trigger))
                else:
                    trigger = 0
                if trigger > 15:
                    print("Process stalled!")
                    ps = subprocess.Popen(['qdel', jobIDs[0]], stdout=subprocess.PIPE)
                    ps.communicate()

                lastTime = currentTime                
                
            break
        attempts = attempts - 1
        sleep(2) 
    if not jobRunning(jobIDs) or terminate: p.kill()
    print "job terminated" 
    (stdoutdata, stderrdata) = p.communicate()

else:
    print("\njob configuration or submission failed\n")

             
print(stdoutdata)
print(stderrdata)
 
############ post-execute #########


for job in jobIDs:
    ps = subprocess.Popen(['qdel', job], stdout=subprocess.PIPE)


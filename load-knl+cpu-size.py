from openfoam import *

#config.table[P.MESH_SIZE] = [ 32, 64, 128, 256, 512, 1024, 2048]
#config.table[P.MESH_TYPE] = ["staticFvMesh", "dynamicRefineExtFvMesh"]

session_path = prepare_folder_structure(__file__)
globalEndTime = 0.100
globalBalanceP = 6
globalMeshType = "staticFvMesh"
globalSolver = "interDyMFoam"
globalCase =  "testCases/2.4.x-damBreakWithObstacle"
gStrontSize = 2048

######################################################################


config = OpenFOAM_Config(session_path, 
                         "662",
                         globalSolver,
                         globalCase)

config.samples=4
config.table[P.LOAD_MANAGER] = [True, False]
config.table[P.COMM_GRAPH] = [False, True]
config.table[P.CPU] = [ (24+64,256), (24+64,512),(24+64,1024), (24+64,2048), (24+64,4096)]
config.table[P.ENDTIME] = [globalEndTime]
config.table[P.BALANCE_PERIOD] = [globalBalanceP]
config.table[P.MESH_TYPE] = [globalMeshType]

config.nodeList=["r662"] # + 64 cores from KNL
config.nodes=1
config.walltimeH=6

batch = openfoam_loadManager_pbs_knl(config)
batch.run(False)
batch.run(True)

